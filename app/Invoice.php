<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

use \DB;

class Invoice extends Model
{
    protected $table = 'invoices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'amount',
        'status',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];
	
	/**
     * Get Invoice revenue Month wise
     */
    public function monthWiseRevenue($get)
    {
		$start_date = (sizeof($get) && $get['start']) ? date("Y-m-d H:i:s", strtotime($get['start'])) : date("Y-m-d H:i:s", strtotime("-1 year", time()));
		$end_date = (sizeof($get) && $get['end']) ? date("Y-m-d H:i:s", strtotime($get['end'])) : date("Y-m-d H:i:s");
        return DB::table('invoices')
               ->select(DB::raw('SUM(amount) as amt'), 'created_at')
				->groupBy(DB::raw('Month(created_at)'))
				->where('created_at', '>=',$start_date)
				->where('created_at', '<=',$end_date)
				->where('status', 'paid')
				->get();
    }
	
	/**
     * Get Jobs Month wise
     */
    public function monthWiseJob($get)
    {
		$start_date = (sizeof($get) && $get['start']) ? date("Y-m-d H:i:s", strtotime($get['start'])) : date("Y-m-d H:i:s", strtotime("-1 month", time()));
		$end_date = (sizeof($get) && $get['end']) ? date("Y-m-d H:i:s", strtotime($get['end'])) : date("Y-m-d H:i:s");
        return DB::table('jobs')
               ->select(DB::raw('COUNT(id) as tot'), 'created_at')
				->groupBy(DB::raw('DAY(created_at)'))
				->where('created_at', '>=',$start_date)
				->where('created_at', '<=',$end_date)
				->get();
    }
}
