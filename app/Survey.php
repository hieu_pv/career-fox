<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'surveys';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'job_id', 'company_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function job()
    {
        return $this->belongsTo('Fox\Job');
    }

    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCompany($query, $companyId)
    {
        return $query->where('company_id', '=', (int)$companyId);
    }

    /**
     * Scope a query to only include popular users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfJob($query, $jobId)
    {
        return $query->where('job_id', '=', (int)$jobId);
    }
}
