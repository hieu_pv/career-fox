<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'country_id'];

    public function jobs() {
        return $this->hasMany('Fox\Job');
    }

    public function cities() {
        return $this->hasMany('Fox\City');
    }

    public function country() {
        return $this->belongsTo('Fox\Country');
    }
}
