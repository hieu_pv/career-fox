<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace Fox\Api\Rivs\Resource;

use Fox\Api\Rivs\AbstractResource;
use Httpful\Http;

/**
 * Class Requisitions
 * @see https://v3.rivs.com/api/requisitions/
 * @method get($id = null)
 * @method create(array $data)
 * @method update($id, array $data)
 * @package App\Api\Rivs\Resource
 */
class Requisitions extends AbstractResource
{
    /**
     * @return string
     */
    public function getUri()
    {
        return '/requisitions';
    }

    /**
     * @param $id
     * @return \Httpful\Response
     */
    public function candidates($id)
    {
        $uri = $this->getUri() . '/' . $id . '/candidates';
        $request = $this->client->request($uri, Http::GET);

        return $request->send();
    }
}