<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace App\Api\Rivs\Resource\Exception;

/**
 * Class ResourceNotFoundException
 * @package App\Api\Rivs\Resource\Exception
 */
class ResourceNotFoundException extends \Exception
{
    protected $message = 'Resource \'%s\' not found';

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->message = sprintf($this->message, $name);
    }
}