<?php
/**
 * @author Sławomir Żytko <slawomir.zytko@gmail.com>
 * @homepage https://github.com/szytko
 */

namespace Fox\Api\Rivs;

use Fox\Api\Rivs\Client;
use Httpful\Http;

/**
 * Class AbstractResource
 * @package App\Api\Rivs
 */
abstract class AbstractResource
{
    /**
     * @returns string
     */
    abstract public function getUri();

    /**
     * @var Client
     */
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $id
     * @returns \Httpful\Response
     */
    public function get($id = null)
    {
        $uri = $this->getUri();
        if ($id) {
            $uri = rtrim($uri, '/') . '/' . $id;
        }
        $request = $this->client->request($uri, Http::GET);
        return $request->send();
    }

    /**
     * @param array $data
     * @return \Httpful\Response
     */
    public function create(array $data)
    {
        $request = $this->client->request($this->getUri(), Http::POST);
        $request->body(json_encode($data));

        return $request->send();
    }

    /**
     * @param $id
     * @param array $data
     * @return \Httpful\Response
     */
    public function update($id, array $data)
    {
        $uri = $uri = rtrim($this->getUri(), '/') . '/' . $id;
        $request = $this->client->request($uri, Http::PUT);
        $request->body($data);

        return $request->send();
    }

    /**
     * @param $id
     * @return \Httpful\Response
     */
    public function delete($id)
    {
        $uri = $uri = rtrim($this->getUri(), '/') . '/' . $id;
        $request = $this->client->request($uri, Http::DELETE);

        return $request->send();
    }
}