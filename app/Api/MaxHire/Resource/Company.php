<?php
namespace Fox\Api\MaxHire\Resource;
use Fox\Api\MaxHire\AbstractResource;

/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 1:21 PM
 */
class Company extends AbstractResource
{
    public function getEmptyForAdd()
    {
        $result = $this->client->Company_GetEmptyForAdd();
        return $result->Company_GetEmptyForAddResult;
    }

    public function get($id)
    {
        $params = [
            'intId'=>(int)$id
        ];
        $body = $this->client->Company_Get($params);

        file_put_contents('company-schema.xml', $body->Company_GetResult->schema);
        file_put_contents('company-any.xml', $body->Company_GetResult->any);
        return $body->Company_GetResult;
    }
}