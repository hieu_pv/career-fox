<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 11:30 PM
 */

namespace Fox\Api\MaxHire\Resource;


class AddResult
{
    public $Success = false;
    public $Id;
    public $ErrorCode;
    public $ErrorDescription;

    static public function fromXml($xml)
    {
        $result = new self();
        $obj = simplexml_load_string($xml);
        if ($obj->Response) {
            $result->Success = ((string)$obj->Response->Success == 'True') ? true : false;
            $result->Id = (int)$obj->Response->Id;
            $result->ErrorCode = (int)$obj->Response->ErrorCode;
            $result->ErrorDescription = (string)$obj->Response->ErrorDescription;
        }

        return $result;
    }

}