<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 1:05 PM
 */

namespace Fox\Api\MaxHire;

abstract class AbstractResource
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * AbstractResource constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }
}