<?php

namespace Fox\Api\MaxHire\DataSet\Job;

/**
 * Created by PhpStorm.
 * User: Charles Oneka
 * Date: 11/1/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\IInterview;

class InterviewDataSetTransformer implements ITransformer
{

    protected $interview;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(IInterview $interview)
    {
        $this->interview = $interview;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        // required for maxhire so we can know what the contact id is of the 
        // user within maxhire.

        $nodes = $xpath->query("//NewDataSet/Table/DeleteFlag");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getDeleteFlag();
        }

        $nodes = $xpath->query("//NewDataSet/Table/ContactsDivision");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getContactsDivision();
        }

        $nodes = $xpath->query("//NewDataSet/Table/active");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getActive();
        }
        $nodes = $xpath->query("//NewDataSet/Table/Category_ID");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getCategoryId();
        }
        $nodes = $xpath->query("//NewDataSet/Table/city");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getCity();
        }

        $nodes = $xpath->query("//NewDataSet/Table/compname");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getCompName();
        }

        // Recruiter
        $nodes = $xpath->query("//NewDataSet/Table/CompRecruiter");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getCompRecruiter();
        }
        // Country
        $nodes = $xpath->query("//NewDataSet/Table/Locale");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getlocale();
        }        
        

        $nodes = $xpath->query("//NewDataSet/Table/phone");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getPhone();
        }

        $nodes = $xpath->query("//NewDataSet/Table/phone_area");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getPhoneArea();
        }

        $nodes = $xpath->query("//NewDataSet/Table/state");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getState();
        }

        $nodes = $xpath->query("//NewDataSet/Table/zip");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getZip();
        }

    }

}