<?php
/**
 * Created by PhpStorm.
 * User: Charles Oneka
 * Date: 10/31/2015
 * Time: 9:18 PM
 */

namespace Fox\Api\MaxHire\DataSet\Company;

use Fox\Api\MaxHire\DataSet\DataSet as AbstractDataSet;
use Fox\ICompany;

class CompanyDataSet extends AbstractDataSet
{
    protected $template = __DIR__ . '/ds-company.xml';
    protected $company;

    /**
     * PersonDataSet constructor.
     */
    public function __construct(ICompany $company)
    {
        $this->company = $company;
        $transformer = new CompanyDataSetTransformer($this->interview);
        parent::__construct($this->template, $transformer);
    }

    public function toString()
    {
        $xml = $this->doc->saveHTML();
        $xml = trim(str_replace(['<root>', '</root>'], ['<ns1:dsCompany>', '</ns1:dsCompany>'], $xml));

        return $xml;
    }
}