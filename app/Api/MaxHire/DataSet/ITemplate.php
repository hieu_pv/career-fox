<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/30/2015
 * Time: 11:26 AM
 */

namespace Fox\Api\MaxHire\DataSet;


interface ITemplate
{
    public function getPath();
}