<?php

namespace Fox\Api\MaxHire\DataSet\Person;

/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\ICandidate;

class PersonDataSetTransformer implements ITransformer
{

    protected $candidate;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(ICandidate $candidate)
    {
        $this->candidate = $candidate;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        $nodes = $xpath->query("//NewDataSet/Table/first");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->candidate->getFirstName();
        }
        $nodes = $xpath->query("//NewDataSet/Table/last");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->candidate->getLastName();
        }
        $nodes = $xpath->query("//NewDataSet/Table/email");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->candidate->getEmail();
        }
    }

}