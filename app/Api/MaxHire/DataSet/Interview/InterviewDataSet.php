<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 9:18 PM
 */

namespace Fox\Api\MaxHire\DataSet\Interview;

use Fox\Api\MaxHire\DataSet\DataSet as AbstractDataSet;
use Fox\IInterview;

class InterviewDataSet extends AbstractDataSet
{
    protected $template = __DIR__ . '/ds-interview.xml';
    protected $interview;

    /**
     * PersonDataSet constructor.
     */
    public function __construct(IInterview $interview)
    {
        $this->interview = $interview;
        $transformer = new InterviewDataSetTransformer($this->job);
        parent::__construct($this->template, $transformer);
    }

    public function toString()
    {
        $xml = $this->doc->saveHTML();
        $xml = trim(str_replace(['<root>', '</root>'], ['<ns1:dsInterview>', '</ns1:dsInterview>'], $xml));

        return $xml;
    }
}