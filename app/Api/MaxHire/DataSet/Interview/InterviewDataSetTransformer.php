<?php

namespace Fox\Api\MaxHire\DataSet\Job;

/**
 * Created by PhpStorm.
 * User: Charles Oneka
 * Date: 11/1/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\IInterview;

class InterviewDataSetTransformer implements ITransformer
{

    protected $interview;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(IInterview $interview)
    {
        $this->interview = $interview;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        // required for maxhire so we can know what the contact id is of the 
        // user within maxhire.

        $nodes = $xpath->query("//NewDataSet/Table/contacts_id");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getContactsId();
        }

        $nodes = $xpath->query("//NewDataSet/Table/date_inter");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getStartDateTime();
        }

        $nodes = $xpath->query("//NewDataSet/Table/date_inter_end");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getEndDateTime();
        }
        $nodes = $xpath->query("//NewDataSet/Table/DateEnter");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getDateEnter();
        }
        $nodes = $xpath->query("//NewDataSet/Table/RefLocation");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getRefLocation();
        }

        $nodes = $xpath->query("//NewDataSet/Table/RefNotes");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getRefNotes();
        }

        // Allows us to see if an interview was refused or cancelled
        $nodes = $xpath->query("//NewDataSet/Table/Refuser_date");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getRefuserDate();
        }
        // Gets reason interview for cancellation
        $nodes = $xpath->query("//NewDataSet/Table/Refuser_text");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->interview->getRefuserText();
        }        
        // should we get cid reference? 

    }

}