<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 9:18 PM
 */

namespace Fox\Api\MaxHire\DataSet\Document;

use Fox\Api\MaxHire\DataSet\DataSet as AbstractDataSet;
use Fox\IDocument;

class DocumentDataSet extends AbstractDataSet
{
    protected $template = __DIR__ . '/ds-document.xml';
    protected $document;

    /**
     * PersonDataSet constructor.
     */
    public function __construct(IDocument $document)
    {
        $this->document = $document;
        $transformer = new DocumentDataSetTransformer($this->job);
        parent::__construct($this->template, $transformer);
    }

    public function toString()
    {
        $xml = $this->doc->saveHTML();
        $xml = trim(str_replace(['<root>', '</root>'], ['<ns1:dsDocument>', '</ns1:dsDocument>'], $xml));

        return $xml;
    }
}