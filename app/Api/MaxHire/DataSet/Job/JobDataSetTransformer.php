<?php

namespace Fox\Api\MaxHire\DataSet\Job;

/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 4:42 PM
 */

use Fox\Api\MaxHire\DataSet\ITransformer;
use Fox\IJob;

class JobDataSetTransformer implements ITransformer
{

    protected $job;

    /**
     * PersonDataSetTransformer constructor.
     */
    public function __construct(IJob $job)
    {
        $this->job = $job;
    }

    public function transform(\DOMDocument $doc)
    {
        $xpath = new \DOMXPath($doc);

        $nodes = $xpath->query("//NewDataSet/Table/job_title");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getTitle();
        }
        $nodes = $xpath->query("//NewDataSet/Table/job_desc");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getDescription();
        }
        $nodes = $xpath->query("//NewDataSet/Table/Jobscity");
        if ($nodes->length > 0) {
            $node = $nodes->item(0);
            $node->nodeValue = $this->job->getCity();
        }
    }

}