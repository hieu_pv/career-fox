<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 9:18 PM
 */

namespace Fox\Api\MaxHire\DataSet\Job;

use Fox\Api\MaxHire\DataSet\DataSet as AbstractDataSet;
use Fox\IJob;

class JobDataSet extends AbstractDataSet
{
    protected $template = __DIR__ . '/ds-job.xml';
    protected $job;

    /**
     * PersonDataSet constructor.
     */
    public function __construct(IJob $job)
    {
        $this->job = $job;
        $transformer = new JobDataSetTransformer($this->job);
        parent::__construct($this->template, $transformer);
    }

    public function toString()
    {
        $xml = $this->doc->saveHTML();
        $xml = trim(str_replace(['<root>', '</root>'], ['<ns1:dsJob>', '</ns1:dsJob>'], $xml));

        return $xml;
    }
}