<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/31/2015
 * Time: 4:33 PM
 */

namespace Fox\Api\MaxHire\DataSet;


interface ITransformer
{
    public function transform(\DOMDocument $doc);
}