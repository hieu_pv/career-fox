<?php
namespace Fox\Api\MaxHire;
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 10/29/2015
 * Time: 12:40 PM
 */
class Error
{
    protected $error = [
        1001 => 'MaxHire credentials were not supplied in the SOAP header.',
        1002 => 'MaxHire credentials could not be authenticated.',
        1010 => 'Data was not found in the dataset supplied.',
        1011 => 'Batch updates are not supported by this method. The dataset must contain a single row of data.',
        1012 => 'Mandatory field was not found: [field name].',
        1013 => 'Field validation error.',
        1014 => 'Primary Key found in dataset does not match the Id value passed in as a method parameter.',
        1015 => 'A record with the specified Id [field name] does not exist or invalid link.',
        1016 => 'The record is deleted.',
        1017 => 'The data contains a field [field name] that is not permitted for updating.',
        1018 => 'A person record with the same name and zipcode already exists. This operation has been aborted.',
        1019 => 'Multiple people records with the same name and zipcode already exist. This operation has been aborted.',
        1020 => 'An error has occurred while attempting to update the data.',
        1021 => 'A person record with the same name and email address already exists. This operation has been aborted.',
        1022 => 'Multiple people records with the same name and email address already exist. This operation has been aborted.',
        1023 => 'A document record with the same title and file size was recently added for this person. This operation has been aborted.',
        1024 => 'MaxHire file can only be retrieved on a letterhead for doc and docx file types.'
    ];

}