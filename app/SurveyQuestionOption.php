<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestionOption extends Model
{
    protected $table = 'survey_question_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'value', 'is_answer', 'survey_question_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function surveyQuestion()
    {
        return $this->belongsTo('Fox\SurveyQuestion');
    }

    public function isAnswer()
    {
        return ($this->is_answer);
    }
}
