<?php

namespace Fox\Transformer;

class OfferLetterResultTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform($entity)
    {
        return [
            'job_id' => (int) $entity->job_id,
            'candidate_ids' => $entity->candidate_ids,
            'message' => $entity->message,
        ];
    }

}