<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\PhoneScreen;

class PhoneScreenTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
        'candidate'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    public $defaultIncludes = [
    ];

    public function transform(PhoneScreen $obj)
    {
        return [
            'id' => (int)$obj->id,
            'candidate_id' => (int)$obj->candidate_id,
            'job_id' => (int)$obj->job_id,
            'quick_note' => $obj->quick_note,
            'resume' => $obj->candidate->resume,
            'name' => $obj->candidate->first_name . ' ' . $obj->candidate->last_name,
            'invited_video_interview' => $obj->is_invited ? true : false,
            'is_shared' => $obj->is_shared ? true : false,
            'message' => isset($obj->message) ? $obj->message : ''
        ];
    }

    public function includeCandidate(PhoneScreen $entity)
    {
        $model = $entity->candidate;

        if (!$model) {
            return null;
        }

        return $this->item($model, new CandidateTransformer());
    }

    public function includeJob(PhoneScreen $entity)
    {
        $model = $entity->job;

        if (!$model) {
            return null;
        }

        return $this->item($model, new JobTransformer());
    }
}