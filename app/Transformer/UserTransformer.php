<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\User;

class UserTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'company'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    public $defaultIncludes = [
    ];

    public function transform(User $entity)
    {        
        return [
            'id' => (int) $entity->id,
            //'username' => $entity->username,
            'email' => $entity->email,
            'name' => $entity->first_name . ' ' . $entity->last_name,
            'first_name' => $entity->first_name,
            'last_name' => $entity->last_name,
            'company_id' => $entity->company_id,
            //'company'=> $entity->company->name,
            'role_name' => $entity->role->name,
            'phone' => $entity->phone
        ];
    }
    
    public function includeCompany(User $entity){
        $model = $entity->company;
        if(!$model){
            return null;
        }

        return $this->item($model, new CompanyTransformer());
    }
    
    public function includeCandidate(User $entity){
        $model = $entity->candidate;
        if(!$model){
            return null;
        }

        return $this->item($model, new CandidateTransformer());
    }
    
}