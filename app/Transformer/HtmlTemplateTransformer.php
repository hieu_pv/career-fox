<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\HtmlTemplate;

class HtmlTemplateTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(HtmlTemplate $entity)
    {
        $return_data = [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'content' => $entity->content,
        ];
        
        if ($entity->attachment_name) {
            $return_data['attachment_url'] = public_path(). '/storage/templates/' . $entity->attachment_name;
        }
        
        return $return_data;
    }
}