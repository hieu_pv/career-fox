<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer\Survey;

use Fox\Transformer\AbstractTransformer;
use Fox\Survey;
use Fox\Transformer\CompanyTransformer;
use Fox\Transformer\JobTransformer;

class SurveyTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'questions',
        'company',
        'job'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(Survey $entity)
    {
        return [
            'id' => (int)$entity->id,
            'title' => $entity->title,
            'description' => $entity->description,
            'job_id' => (int)$entity->job_id,
            'company_id' => (int)$entity->company_id,
            'created_at' => $this->formatDate($entity->created_at),
            'updated_at' => $this->formatDate($entity->updated_at)
        ];
    }

    public function includeQuestions(Survey $entity)
    {
        $model = $entity->questions;
        if (!$model) {
            return null;
        }
        return $this->collection($model, new SurveyQuestionTransformer());
    }

    public function includeJob(Survey $entity)
    {
        $model = $entity->job;
        if (!$model) {
            return null;
        }
        return $this->item($model, new JobTransformer());
    }

    public function includeCompany(Survey $entity)
    {
        $model = $entity->company;
        if (!$model) {
            return null;
        }
        return $this->item($model, new CompanyTransformer());
    }
}