<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\OfferLetter;

class OfferLetterTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'job',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
    ];

    public function transform(OfferLetter $entity)
    {
        return [
            'id' => (int) $entity->id,
            'job_id' => (int) $entity->job_id,
            'candidate_id' => (int) $entity->candidate_id,
            //'offer_letter' => $entity->offer_letter
        ];
    }
    
    public function includeJob(OfferLetter $entity)
    {
        $model = $entity->job;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new JobTransformer());
    }
    
    public function includeHtmlTemplate(OfferLetter $entity)
    {
        $model = $entity->html_template;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new HtmlTemplateTransformer());
    }
}