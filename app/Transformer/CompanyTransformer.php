<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 7:00 AM
 */

namespace Fox\Transformer;

use Fox\Company;

class CompanyTransformer extends AbstractTransformer
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //'user'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'user'
    ];

    public function transform(Company $entity)
    {
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'email' => $entity->email,
            'phone' => $entity->phone,
            'description' => $entity->description,
            'primary_contact' => $entity->primary_contact
        ];
    }

    public function includeUser(Company $entity)
    {
        $model = $entity->user;
        if ( ! $model) {
            return null;
        }
        return $this->item($model, new UserTransformer());
    }
}