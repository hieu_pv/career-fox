<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class PhoneScreen extends Model
{
    protected $table = 'phone_screens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['candidate_id', 'user_id', 'job_id', 'quick_note', 'is_shared', 'is_invited'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
    
    public function candidate() {
        return $this->belongsTo('Fox\Candidate');
    }
}
