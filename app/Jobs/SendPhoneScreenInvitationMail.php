<?php

namespace Fox\Jobs;

use Fox\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use Fox\Candidate;

class SendPhoneScreenInvitationMail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $candidateIds;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($candidateIds = array())
    {
        $this->candidateIds = $candidateIds;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->candidateIds AS $id) {
            $candidate = Candidate::find($id);
            // send email
            Mail::send('emails.phone_invitations', ['data' => $candidate], function ($m) use ($candidate) {
                $m->to($candidate->email, $candidate->name)->subject('You are invited to phone screen!');
            });
        }
    }
}
