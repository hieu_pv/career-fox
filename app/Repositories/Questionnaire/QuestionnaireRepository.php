<?php
namespace Fox\Repositories\Questionnaire;

use Fox\Repositories\AbstractRepository;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

use Fox\Questionnaire;
use Fox\Candidate;
use Fox\Interview;
use Fox\ReferenceCheck;

class QuestionnaireRepository extends AbstractRepository
{
    protected $rules = [
        'candidate_id' => 'required|integer',
        'job_id' => 'required|integer',
        'notes' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        'job_id',
        'candidate_id'
    ];

    protected $model;

    public function __construct(Questionnaire $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new QuestionnaireSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Questionnaire::query();
        
        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }*/
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Questionnaire::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return Questionnaire::destroy($id);
    }
    
    public function getCandidatesInQuestionnaire($job_id)
    {
        $candidates = Candidate::join('questionnaires', 'candidates.id', '=', 'questionnaires.candidate_id')
                ->where('questionnaires.job_id', $job_id)
                ->select('candidates.*', DB::raw('questionnaires.id AS questionnaire_id, questionnaires.is_invited'))
                ->paginate(15);
        
        return $candidates;
    }
    
    /**
     * Invite a candidate to questionnaire
     */
    public function invite($id, $model)
    {
        try {
            global $user;
            
            if ($model->is_invited) {
                return false;
            }

            $interview = Interview::firstOrCreate([
                'questionnaire_id' => $id,
                'candidate_id' => $model->candidate_id,
                'job_id' => $model->job_id
            ]);
            
            ReferenceCheck::firstOrCreate([
                'candidate_id' => $model->candidate_id,
                'job_id' => $model->job_id,
                'user_id' => $user->id
            ]);
            
            $interview->user_id = $user->id;
            $interview->save();
            
            $model->is_invited = 1;
            $model->save();

            return $interview;
            // send email to candidate
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}