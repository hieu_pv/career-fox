<?php
namespace Fox\Repositories\ReferenceCheckContact;

use Fox\Repositories\AbstractRepository;
use Fox\ReferenceCheckContact;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

class ReferenceCheckContactRepository extends AbstractRepository
{
    protected $rules = [
        'name' => 'required|min:3',
        'phone' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        'title',
    ];

    protected $model;

    public function __construct(ReferenceCheckContact $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null, $reference_id)
    {
        $searchFields = new ReferenceCheckContactSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = ReferenceCheckContact::where('reference_check_id', $reference_id);
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('name', 'LIKE', $keyword);
                $q->orWhere('phone', 'LIKE', $keyword);
            });
        }
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($reference_check_id, $id)
    {
        return ReferenceCheckContact::where('reference_check_id', $reference_check_id)->where('id', $id)->first();
    }

    public function findByIds(array $ids)
    {
        return ReferenceCheckContact::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return ReferenceCheckContact::destroy($id);
    }
}