<?php
namespace Fox\Repositories\VideoInterview;

use Fox\Repositories\AbstractRepository;
use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;

use Fox\VideoInterview;
use Fox\Candidate;
use Fox\Questionnaire;
use DB;


class VideoInterviewRepository extends AbstractRepository
{
    protected $rules = [
        'question' => 'required|min:10',
        'job_id' => 'required|integer'
    ];


    protected $errors;

    protected $sortFields = [
        'question',
    ];

    protected $model;

    public function __construct(VideoInterview $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new VideoInterviewSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = VideoInterview::query();
        
        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('question', 'LIKE', $keyword);
            });
        }*/
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return VideoInterview::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, ['quick_note' => 'required|min:2']);
            
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return VideoInterview::destroy($id);
    }
    
    public function getCandidatesInVideoInterview($job_id)
    {
        $candidates = Candidate::join('video_interviews', 'candidates.id', '=', 'video_interviews.candidate_id')
                ->where('video_interviews.job_id', $job_id)
                ->select('candidates.*', DB::raw('video_interviews.id AS video_interview_id, video_interviews.is_invited AS invited_questionnaire'))
                ->paginate(15);
        
        return $candidates;
    }
    
    /**
     * Invite a candidate to questionnaire
     */
    public function invite($id, $model)
    {
        try {
            global $user;
            
            if ($model->is_invited) {
                return false;
            }

            $questionnaire = Questionnaire::firstOrCreate([
                'video_interview_id' => $id,
                'candidate_id' => $model->candidate_id,
                'job_id' => $model->job_id
            ]);
            $questionnaire->user_id = $user->id;
            $questionnaire->save();
            
            $model->is_invited = 1;
            $model->save();

            return $questionnaire;
            // send email to candidate
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}