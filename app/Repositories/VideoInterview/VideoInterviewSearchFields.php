<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\VideoInterview;

use Fox\Repositories\Query\TraitSearchFields;

class VideoInterviewSearchFields
{
    use TraitSearchFields;

    public $keyword;
}