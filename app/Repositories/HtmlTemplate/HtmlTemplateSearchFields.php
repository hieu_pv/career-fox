<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\HtmlTemplate;

use Fox\Repositories\Query\TraitSearchFields;

class HtmlTemplateSearchFields
{
    use TraitSearchFields;

    public $keyword;
}