<?php
namespace Fox\Repositories\Query;

trait TraitSearchFields
{
    public function __construct(array $fields = [])
    {
        $this->setFields($fields);
    }

    public function setFields(array $fields)
    {
        $vars = static::getSearchFields();
        foreach ($fields as $k => $v) {
            if (!is_string($v)) {
                continue;
            }
            if (in_array($k, $vars)) {
                $this->$k = $v;
            }
        }
    }

    /**
     * Get available fields for search
     * @return array
     */
    public static function getSearchFields()
    {
        return array_keys(get_class_vars(__CLASS__));
    }
}