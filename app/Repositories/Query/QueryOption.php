<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/29/2015
 * Time: 6:39 AM
 */

namespace Fox\Repositories\Query;


class QueryOption
{
    const PAGE_SIZE = 20;
    public $page = 1;
    public $pageSize = self::PAGE_SIZE;
    public $limit;
    public $offset;
    public $ids;
    public $sortBy;
    public $sortOrder;
    public $isDeleted;

    private function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }


    /**
     * @return QueryOption
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @param $limit
     *
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return (int)$this->limit;
    }

    /**
     * @param $offset
     *
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @param $ids
     *
     * @return $this
     */
    public function filterByIds($ids)
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @param $sortBy
     *
     * @return $this
     */
    public function sortBy($sortBy)
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param $sortOrder
     *
     * @return $this
     */
    public function sortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function isDeleted($value)
    {
        $this->isDeleted = $value;
        return $this;
    }

}