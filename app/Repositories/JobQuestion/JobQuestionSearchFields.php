<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\JobQuestion;

use Fox\Repositories\Query\TraitSearchFields;

class JobQuestionSearchFields
{
    use TraitSearchFields;

    public $keyword;
}