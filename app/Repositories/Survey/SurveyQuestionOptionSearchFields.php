<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Survey;

use Fox\Repositories\Query\TraitSearchFields;

class SurveyQuestionOptionSearchFields
{
    use TraitSearchFields;

    public $keyword;
    public $question_id;
}