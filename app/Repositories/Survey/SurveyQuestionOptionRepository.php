<?php
namespace Fox\Repositories\Survey;

use Fox\Repositories\AbstractRepository;
use Fox\Repositories\Query\QueryOption;
use Fox\SurveyQuestionOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

class SurveyQuestionOptionRepository extends AbstractRepository
{
    protected $rules = [
        'title' => 'required|min:3',
        'question_id' => 'integer|required|exists:survey_questions,id',
    ];


    protected $errors;

    protected $sortFields = [
        'title',
    ];

    protected $model;

    public function __construct(SurveyQuestionOption $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new SurveyQuestionOptionSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = SurveyQuestionOption::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }
        if ($searchFields->survey_id) {
            $query->where('survey_id', '=', (int)$searchFields->survey_id);
        }

        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return SurveyQuestionOption::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        if (!isset($data['value']) || !$data['value']) {
            $data['value'] = $data['title'];
        }
        if (!isset($data['is_answer'])) {
            $data['is_answer'] = false;
        } else {
            $data['is_answer'] = (bool)$data['is_answer'];
        }

        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return SurveyQuestionOption::destroy($id);
    }
}