<?php
namespace Fox\Repositories\OfferLetter;

use Fox\Repositories\AbstractRepository;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

use Fox\OfferLetter;
use Fox\Candidate;
use Mail;

class OfferLetterRepository extends AbstractRepository
{
    protected $rules = [
        'job_id' => 'required|integer',
        'offer_letter' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        'job_id'
    ];

    protected $model;

    public function __construct(OfferLetter $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new OfferLetterSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = OfferLetter::query();
        
        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('offer_letter', 'LIKE', $keyword);
            });
        }*/
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return OfferLetter::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return OfferLetter::destroy($id);
    }
    
    public function getCandidatesInOfferLetter($job_id)
    {
        $candidates = Candidate::join('offer_letters', 'candidates.id', '=', 'offer_letters.candidate_id')
                ->where('offer_letters.job_id', $job_id)
                ->select('candidates.*', DB::raw('offer_letters.id AS offer_letter_id'))
                ->paginate(15);
        
        return $candidates;
    }
    
    public function sendOffers($job_id, $data)
    {
        try {
            global $user;

            $validator = Validator::make($data, [
                'offer_letter' => 'required',
                'candidate_ids' => 'required|array'
            ]);
            
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }

            $candidate_ids = $data['candidate_ids'];
            $candidate_arr = array_filter($candidate_ids, function($value) {
                return (int) $value;
            });

            $candidate_arr = array_unique($candidate_arr);

            // get all candidates of the the current job thoese are invited to offer letter

            $candidate_number = OfferLetter::where('job_id', $job_id)->whereIn('candidate_id', $candidate_arr)->count();

            if ( $candidate_number != count($candidate_ids) ) {
                $this->errors = 'invalid candidate_ids';
                return false;
            }
            
            $candidates = Candidate::whereIn('id', $candidate_arr)->get();
            
            foreach ($candidates AS $candidate) {
                //$candidate->employee_fname = $employee->first_name;
                // send email
                Mail::raw($data['offer_letter'], function ($m) use ($candidate) {
                    $m->to($candidate->email, $candidate->name)->subject('You received an offer letter!');
                    $m->to('sunaroad@gmail.com', $candidate->name)->subject('You received an offer letter!');
                });
            }

            return true;

        } catch (\Exception $ex) {
            throw $ex;
        }
    }
}