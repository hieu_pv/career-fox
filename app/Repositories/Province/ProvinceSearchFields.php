<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 8/30/2015
 * Time: 8:37 AM
 */

namespace Fox\Repositories\Province;

use Fox\Repositories\Query\TraitSearchFields;

class ProvinceSearchFields
{
    use TraitSearchFields;

    public $keyword;
}