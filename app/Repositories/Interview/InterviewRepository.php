<?php
namespace Fox\Repositories\Interview;

use Fox\Repositories\AbstractRepository;
use Fox\Interview;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

use Fox\Candidate;
use Input;

class InterviewRepository extends AbstractRepository
{
    protected $rules = [
        'quick_note' => 'required|min:3'
    ];


    protected $errors;

    protected $sortFields = [
        'job_id',
        'candidate_id'
    ];

    protected $model;

    public function __construct(Interview $model)
    {
        $this->model = $model;
        
        $this->interview_note_path = public_path() . '/storage/interview_notes/';
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null)
    {
        $searchFields = new InterviewSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = Interview::query();
        
        /*if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('title', 'LIKE', $keyword);
            });
        }*/
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return Interview::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        try {
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            if (!$entity->update($item)) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($id)
    {
        return Interview::destroy($id);
    }
    
    public function getCandidatesInInterview($job_id)
    {
        $candidates = Candidate::join('interviews', 'candidates.id', '=', 'interviews.candidate_id')
                ->where('interviews.job_id', $job_id)
                ->select('candidates.*', DB::raw('interviews.id AS interview_invitation_id'))
                ->paginate(15);
        
        return $candidates;
    }
    
    public function upload($model, $data)
    {
        try {
            global $user;
            
            $rules = [
                'note' => 'required|mimes:doc,docx,pdf|max:2000'
            ];
        
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            // upload file
            $note = Input::file('note');
            $model->attachment_name = $this->upload_file($note, $this->interview_note_path);
            $model->save();
            
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }
    
    protected function upload_file($file, $path) {
        if ($file) {
            $file_name = $file->getClientOriginalName();

            $i = 1;
            $file_path = $path . $file_name;
            while ( file_exists($file_path) ) {
                $file_name = $i . $file->getClientOriginalName();
                $file_path = $path . $file_name;
                $i++;
            }

            $file->move($path, $file_name);

            return $file_name;
        }
        
        return '';
    }
}