<?php
namespace Fox\Repositories\User;

use Fox\Repositories\AbstractRepository;
use Fox\User;

use Fox\Repositories\Query\QueryOption;
use Illuminate\Pagination\Paginator;
use Validator;
use DB;

use Fox\Http\Controllers\UserController;

class UserRepository extends AbstractRepository
{
    protected $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users,email',
        //'username' => 'required|unique:users,username',
        'password' => 'required|min:6'
    ];


    protected $errors;

    protected $sortFields = [
        'title',
    ];

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }


    public function errors()
    {
        return $this->errors;
    }

    public function all(array $search = array(), QueryOption $searchOptions = null, $option = false)
    {
        global $user;
        
        $searchFields = new UserSearchFields($search);
        Paginator::currentPageResolver(function () use ($searchOptions) {
            return $searchOptions->getPage();
        });

        $query = User::query();
        if ($searchFields->keyword) {
            $keyword = '%' . $searchFields->keyword . '%';
            $query->where(function ($q) use ($keyword) {
                $q->orWhere('first_name', 'LIKE', $keyword);
                $q->orWhere('last_name', 'LIKE', $keyword);
                $q->orWhere('name', 'LIKE', $keyword);
                $q->orWhere('email', 'LIKE', $keyword);
                $q->orWhere('phone', 'LIKE', $keyword);
            });
        }
        
        if ($option) {
            $query->where('company_id', $option['company_id']);
            $query->where('id', '<>', $option['user_id']);
        }
        
        $sortBy = $searchOptions->getSortBy();
        if ($sortBy && in_array($sortBy, $this->sortFields)) {
            $query = $query->orderBy($sortBy, $searchOptions->getSortOrder());
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }
        $query = $query->paginate($searchOptions->getLimit());
        return $query;
    }

    public function find($id)
    {
        return $this->getById($id);
    }

    public function findByIds(array $ids)
    {
        return User::whereIn('id', $ids);
    }

    private function prepare(array $data)
    {
        return $data;
    }

    public function create(array $data)
    {
        global $user;
        
        try {
            $this->rules['role'] = 'required|in:admin,manager,recruiter,employee';
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $role = $data['role'];
            
            if ($role == 'admin') {
                if ( ! $user->hasRole('admin')) {
                    $this->errors = 'Only admin role can create another admin role';
                    return false;
                }
                $data['role_id'] = 1;
            } else if ($role == 'manager') {
                if ( ! $user->hasRole('admin') && ! $user->hasRole('manager') ) {
                    $this->errors = 'Only admin or manager roles can create a manager role';
                    return false;
                }
                $data['role_id'] = 3;
            } else if ($role == 'recruiter') {
                $this->rules['company_id'] = 'required|exists:companies,id';
                
                if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                    $data['company_id'] = $user->company_id;
                } else if ($user->hasRole('employee') ) {
                    $this->errors = 'Permission denied';
                    return false;
                }
                
                $data['role_id'] = 7;
            } else if ($role == 'employee') {
                $this->rules['company_id'] = 'required|exists:companies,id';
                
                if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                    $data['company_id'] = $user->company_id;
                } else if ($user->hasRole('employee') ) {
                    $this->errors = 'Permission denied';
                    return false;
                }
                
                $data['role_id'] = 9;
            }
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data);
            $model = $this->model->newInstance($item);
            $model->save();
            if (!$model->id) {
                return false;
            }
            return $model;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function update($entity, array $data)
    {
        global $user;
        
        try {
            $this->rules['email'] = 'required|email|unique:users,email,' . $entity->id;
            
            $role = $entity->role->name;
            
            if ($role == 'admin') {
                if ( ! $user->hasRole('admin')) {
                    $this->errors = 'Only admin role can update admin role';
                    return false;
                }
                
            } else if ($role == 'manager') {
                if ( ! $user->hasRole('admin') && ! $user->hasRole('manager') ) {
                    $this->errors = 'Only admin or manager roles can update manager role';
                    return false;
                }
                
            } else if ($role == 'recruiter') {
                if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                    if ($entity->company_id != $user->company_id) {
                        $this->errors = 'Company and Recruiter can only update recruiter role in your company';
                        return false;
                    }
                } else if ($user->hasRole('employee')) {
                    $this->errors = 'Employee cans only update yourself';
                    return false;
                }
                
            } else if ($role == 'employee') {
                if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                    if ($entity->company_id != $user->company_id) {
                        $this->errors = 'Company and Recruiter can only update employee role in your company';
                        return false;
                    }
                } else if ($user->hasRole('employee')) {
                    if ($user->id != $entity->id) {
                        $this->errors = 'You do not have permission to update other users';
                        return false;
                    }
                }
            } else {
                $this->errors = 'Permission denied';
                return false;
            }
            
            $validator = Validator::make($data, $this->rules);
            if ($validator->fails()) {
                $this->errors = $validator->errors();
                return false;
            }
            
            $item = $this->prepare($data, false);
            
            $entity->first_name = $data['first_name'];
            $entity->last_name = $data['last_name'];
            $entity->phone = isset($data['phone']) ? $data['phone'] : '';
            $entity->email = $data['email'];
            
            if (!$entity->save()) {
                return false;
            }
            return $entity;
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function delete($entity, $id)
    {
        global $user;
        
        $role = $entity->role->name;
            
        if ($role == 'admin') {
            if ( ! $user->hasRole('admin')) {
                $this->errors = 'Only admin role can delete admin role';
                return false;
            }

        } else if ($role == 'manager') {
            if ( ! $user->hasRole('admin') && ! $user->hasRole('manager') ) {
                $this->errors = 'Only admin or manager roles can delete manager role';
                return false;
            }

        } else if ($role == 'recruiter') {
            if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                if ($entity->company_id != $user->company_id) {
                    $this->errors = 'Company and Recruiter can only delete recruiter role in your company';
                    return false;
                }
            } else if ($user->hasRole('employee')) {
                $this->errors = 'Employee doesn not have permission on this task';
                return false;
            }

        } else if ($role == 'employee') {
            if ($user->hasRole('company') || $user->hasRole('recruiter') ) {
                if ($entity->company_id != $user->company_id) {
                    $this->errors = 'Company and Recruiter can only delete employee role in your company';
                    return false;
                }
            } else if ($user->hasRole('employee')) {
                $this->errors = 'Employee doesn not have permission on this task';
                return false;
            }
        } else {
            $this->errors = 'Permission denied';
            return false;
        }
        return User::destroy($id);
    }
}