<?php

namespace Fox\Http\Requests;

use Fox\Http\Requests\Request;

class CreateCandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            //'username' => 'required|unique:users,username',
            'password' => 'required',
            'phone' => 'required|min:8',
            'resume' => 'required|mimes:pdf,doc,docx',
            'cover_letter' => 'required|mimes:pdf,doc,docx'
        ];
    }
    
}
