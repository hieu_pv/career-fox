<?php

namespace Fox\Http\Requests;

use Fox\Http\Requests\Request;

class CreateInvoiceRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'amount' => 'required',
            'status' => 'required|in:pending,sent,paid,cancelled',
        ];
    }
    
    public function response(array $errors) {
        return response()->json(['message' => $errors, 'code' => 422], 422);
    }
}
