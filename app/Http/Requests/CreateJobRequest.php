<?php

namespace Fox\Http\Requests;

use Fox\Http\Requests\Request;

class CreateJobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'postal' => 'required',
            'city' => 'required',
            'province' => 'required',
            'description' => 'required',
            'company_id' => 'required',
            'category_id' => 'required',
        ];
    }
    
    public function response(array $errors) {
        return response()->json(['message' => $errors, 'code' => 422], 422);
    }
}
