<?php

namespace Fox\Http\Controllers\Frontend;

use Fox\Events\UserHasAppliedJob;
use Fox\Events\CandidateWasCreated;
use Fox\Http\Requests\CreateCandidateRequest;
use Fox\Http\Controllers\Controller;
//use Fox\Api\Rivs\Video\Crawler;
//use Fox\Api\Rivs\HtmlFormatter;

use Input;
use Fox\Job;
use Fox\Country;
use Fox\Province;
use Fox\City;
use Fox\Candidate;
use Fox\User;

use Auth;
use DB;
use View;

use Response;
use Event;
use Config;

class JobController extends Controller
{


    public function __construct()
    {
        View::composer(array('thank_you', 'job_detail', 'job_confirmation', 'job_list', 'thankyou'), function ($view) {
            $view->with('user', Auth::user());
        });

        $this->cover_letter_path = public_path() . '/storage/cover_letters/';
        $this->resume_path = public_path() . '/storage/resumes/';
    }

    public function index()
    {
        $jobs = Job::where('status', 1)->paginate(15);
        //dd($jobs[0]);
        return view('job_list', ['jobs' => $jobs]);
    }

    public function show($id)
    {
        $job = Job::find($id);
        if (!$job) {
            return 'Job not found';
        }

        $countries_list = Country::all();
        $countries = ['0' => 'Select'];

        foreach ($countries_list AS $country) {
            $countries[$country->id . ''] = $country->name;
        }

        $provinces = ['0' => 'Select'];
        if (Input::old('country') > 0) {
            $provinces_list = $this->getProvincesByCountry((int)Input::old('country'));
            foreach ($provinces_list AS $province) {
                $provinces[$province->id . ''] = $province->name;
            }
        }

        $cities = ['0' => 'Select'];
        if (Input::old('province') > 0) {
            $cities_list = $this->getCitiesByProvince((int)Input::old('province'));
            foreach ($cities_list AS $city) {
                $cities[$city->id . ''] = $city->name;
            }
        }

        return view('job_detail', ['job' => $job, 'countries' => $countries, 'provinces' => $provinces, 'cities' => $cities]);
    }

    public function confirm($job_id)
    {
        return view('job_confirmation', ['job_id' => $job_id]);
    }

    public function applyJob($job_id)
    {
        // check if user logged in
        $user = Auth::user();
		
        if ($user) {
		
            $job = Job::find($job_id);
            $is_applied = DB::table('candidate_jobs')->where('job_id', $job->id)->where('user_id', $user->id)->count();
				
				// Add condidate to Riv's requisition
				 $result = $this->addtoRivsRequisition($user, $job->requisition_id);
			
    $updateCandidateJob = DB::table('candidate_jobs')->where('job_id', $job->id)->where('user_id', $user->id)->update(['req_candidate_id' => $result->aaRequisitionCandidate->iId]);
            if ($is_applied > 0) {
                return redirect(route('thankyou'))->withMessage('You are already applied for this job');
            } else {
                User::find($user->id)->jobs()->attach($job->id, ['company_id' => $job->company_id]);
				
                /**
                 * Fire event when a candidate applied a job
                 */
                Event::fire(new UserHasAppliedJob($user, $job));

                return redirect(route('thankyou'))->withMessage('You applied for this job successfully');
            }

        } else {
            return redirect(url('demo/jobs/' . $job_id . '/confirm'));
        }
    }

    public function registerCandidate($id, CreateCandidateRequest $request)
    {
        $job = Job::find($id);
        if (!$job) {
            return 'Job not found';
        }

        $post = $request->all();

        // upload resume
        $resume = Input::file('resume');
        $post['resume'] = $this->upload_file($resume, $this->resume_path);

        // upload resume
        $cover_letter = Input::file('cover_letter');
        $post['cover_letter'] = $this->upload_file($cover_letter, $this->cover_letter_path);

        $candidate = new Candidate;
        $candidate = $candidate->createCandidateFromFrontend($post);

        // apply candidate to job
        User::find($candidate->user_id)->jobs()->attach($job->id, ['company_id' => $job->company_id]);
		 // Add condidate to Riv's requisition
		 $result = $this->addtoRivsRequisition($candidate, $job->requisition_id);
    $updateCandidateJob = DB::table('candidate_jobs')->where('job_id', $job->id)->where('user_id', $candidate->user_id)->update(['req_candidate_id' => $result->aaRequisitionCandidate->iId]);


        // send email to user

        // force login
        Auth::loginUsingId($candidate->user_id);

        /**
         * Fire event when a new candidate was created
         */
        Event::fire(new CandidateWasCreated($candidate));

        return redirect(route('thankyou'))->withMessage('You applied for this job successfully');
    }

    public function thankYou()
    {
        return view('thank_you');
    }

    public function logOut()
    {
        Auth::logout();
        return redirect(url('demo'));
    }

    public function login()
    {
        $post = Input::all();
        $email = $post['email'];
        $password = $post['password'];
        $job_id = $post['job_id'];

        $user = Auth::attempt(['password' => $password, 'email' => $email, 'role_id' => 11]);
        if ($user) {
            return redirect(url('demo/jobs/' . $job_id))->withMessage('You logged in successfully. Please apply job now');
        } else {
            return redirect(url('demo/jobs/' . $job_id . '/confirm'))->withMessage('Login fail');
        }
    }

    public function getProvincesByCountry($id)
    {
        return Country::find($id)->provinces;
    }

    public function getCitiesByProvince($id)
    {
        return Province::find($id)->cities;
    }

    protected function upload_file($file, $path)
    {
        if ($file) {
            $file_name = $file->getClientOriginalName();

            $i = 1;
            $file_path = $path . $file_name;
            while (file_exists($file_path)) {
                $file_name = $i . $file->getClientOriginalName();
                $file_path = $path . $file_name;
                $i++;
            }

            $file->move($path, $file_name);

            return $file_name;
        }

        return '';
    }

    public function getJobFeed()
    {
        $jobs = Job::where('status', 1)->get();

        $content = View::make('jobfeed')->with('jobs', $jobs);

        return Response::make($content, '200')->header('Content-Type', 'text/xml');
    }
	
	protected function addtoRivsRequisition($user, $requisition_id)
	{
		if(empty($requisition_id))
			return false;
			
		$rivsApiConfig = Config::get('rivs.api');
		$parameters = array(
		   'iRequisition' => $requisition_id,
		   'sNameFirst' => $user->first_name,
		   'sNameLast' => $user->last_name,
		   'sEmail' => $user->email,
		   'sPhone' => $user->phone    	
		);		
		$json_data = json_encode($parameters);
		$post = file_get_contents($rivsApiConfig['uri'].'requisitionCandidates/',null,stream_context_create(array(
			'http' => array(
			'protocol_version' => 1.1,
			'method'           => 'POST',
			'header'           => "Content-type: application/json\r\n".
			"Connection: close\r\n" .
			"Content-length: " . strlen($json_data) . "\r\n"."Authorization:".$rivsApiConfig['token'],
			'content'          => $json_data,
			),
			 "ssl"=>array(
				"verify_peer"=>false,
				"verify_peer_name"=>false,
			),
		)));
		return $result = json_decode($post);		
	}
}

