<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CategoryTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Category\CategoryRepository;
use Input;


class AutocompleteController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, CategoryRepository $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new CategoryTransformer());
    }
}
