<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\ReferenceCheckContactTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\ReferenceCheckContact\ReferenceCheckContactRepository;
use Input;

use Fox\ReferenceCheck;


class ReferenceCheckContactController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, ReferenceCheckContactRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($reference_check_id)
    {
        global $user;
        
        $entity = ReferenceCheck::find($reference_check_id);
        
        if ( ! $entity ) {
            return $this->response->errorWrongArgs('The Reference Check Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions, $reference_check_id);
        return $this->response->withPaginator($items, new ReferenceCheckContactTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($reference_check_id, $id)
    {
        global $user;
        
        $entity = $this->repository->find($reference_check_id, $id);
        if (!$entity) {
            return $this->response->errorNotFound('Contact could not found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        return $this->response->withItem($entity, new ReferenceCheckContactTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($reference_check_id)
    {
        global $user;
        
        try {
            $entity = ReferenceCheck::find($reference_check_id);
            if ( ! $entity ) {
                return $this->response->errorWrongArgs('The Reference Check Object could not be found');
            }
            
            // check edit own permission
            if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
                return $this->response->errorForbidden('You have not permission on this job');
            }

            $post = Input::all();
            //$post['user_id'] = $this->getCurrentUser()->id;
            $post['reference_check_id'] = $reference_check_id;
            
            if ($model = $this->repository->create($post)) {
                //return $this->response->withItem($model, new LoanTransformer());
                return $this->response->withItem($model, new ReferenceCheckContactTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($reference_check_id, $id)
    {
        global $user;
        
        $entity = $this->repository->find($reference_check_id, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Contact could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
            
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new ReferenceCheckContactTransformer());
        }
        
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($reference_check_id, $id)
    {
        global $user;
        
        $entity = $this->repository->find($reference_check_id, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Contact could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }

    public function authorize($args = NULL)
    {
        return true;
    }    
}
