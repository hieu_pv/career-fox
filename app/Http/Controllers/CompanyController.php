<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CompanyTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Company\CompanyRepository;
use Input;


class CompanyController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, CompanyRepository $repository)
    {
        //$this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new CompanyTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ($user->hasRole('company') && $user->company_id != $id ) {
            return $this->response->errorForbidden('You do not have permission on this company');
        }
        
        return $this->response->withItem($entity, new CompanyTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $post = Input::all();
            $post['user_id'] = $this->getCurrentUser()->id;
            
            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new CompanyTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        global $user;

        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        
        // check edit own permission
        if ($user->hasRole('company') && $user->company_id != $id ) {
            return $this->response->errorForbidden('You do not have permission on this company');
        }
        
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new CompanyTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $this->repository->delete($entity, $id);
        return $this->response->withArray(['id' => $id]);
    }
}
