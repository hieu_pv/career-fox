<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\ReferenceCheckTransformer;
use Fox\Transformer\CandidateReferenceCheckTransformer;
use Fox\Transformer\OfferLetterTransformer;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\ReferenceCheck\ReferenceCheckRepository;
use Input;


class ReferenceCheckController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, ReferenceCheckRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        return $this->response->withItem($entity, new ReferenceCheckTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('video_interview_id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new ReferenceCheckTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    
    /**
     * get list of candidates that are invited to video interview
     */
    public function getCandidatesInReferenceCheck($job_id) {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $candidates = $this->repository->getCandidatesInReferenceCheck($job_id);
        
        return $this->response->withPaginator($candidates, new CandidateReferenceCheckTransformer());
    }
    
    /**
     * Invite a candidate to questionnaire
     */
    public function invite($reference_check_id)
    {
        global $user;

        $entity = $this->repository->find($reference_check_id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Reference Check Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $offer_letter = $this->repository->invite($reference_check_id, $entity);

        if ($offer_letter) {
            return $this->response->withItem($offer_letter, new OfferLetterTransformer());
        } else {
            return $this->response->errorUnwillingToProcess('This candidate is already invited to offer letter. He can not be invited again');
        }
    }
}
