<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\JobQuestionTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\JobQuestion\JobQuestionRepository;
use Input;
use Fox\Job;


class JobQuestionController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, JobQuestionRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($job_id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions, $job_id);
        return $this->response->withPaginator($items, new JobQuestionTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($job_id, $id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }

        $entity = $this->repository->find($job_id, $id);
        if (!$entity) {
            return $this->response->errorNotFound('Question could not found');
        }
        return $this->response->withItem($entity, new JobQuestionTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($job_id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        try {
            $post = [];
            $post['title'] = Input::get('title');
            $post['default_answer'] = Input::get('default_answer');
            $post['user_id'] = $this->getCurrentUser()->id;
            $post['job_id'] = $job_id;
            
            if ($model = $this->repository->create($post)) {
                //return $this->response->withItem($model, new LoanTransformer());
                return $this->response->withItem($model, new JobQuestionTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($job_id, $id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $entity = $this->repository->find($job_id, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Question could not be found');
        }
        
        $post = [];
        $post['title'] = Input::get('title');
        $post['default_answer'] = Input::get('default_answer');
        $post['user_id'] = $this->getCurrentUser()->id;
        $post['job_id'] = $job_id;
            
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new JobQuestionTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($job_id, $id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $entity = $this->repository->find($job_id, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Question could not be found');
        }
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }

}
