<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\CandidateOfferLetterTransformer;
use Fox\Transformer\OfferLetterResultTransformer;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\OfferLetter\OfferLetterRepository;
use Input;


class OfferLetterController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, OfferLetterRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new OfferLetterTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        return $this->response->withItem($entity, new OfferLetterTransformer());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $entity->job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new OfferLetterTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * get list of candidates that are invited to offer letter
     */
    public function getCandidatesInOfferLetter($job_id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $candidates = $this->repository->getCandidatesInOfferLetter($job_id);
        
        return $this->response->withPaginator($candidates, new CandidateOfferLetterTransformer());
    }
    
    public function sendOffers($job_id)
    {
        global $user;
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
        $result = $this->repository->sendOffers($job_id, $post);

        $errors = $this->repository->errors();

        if ($errors) {
            $msg = 'Input data was wrong';
            if (is_object($errors)) {
                $msg = $msg . ' [' . $errors->first() . ']';
            } else {
                $msg = $msg . ' [' .$errors . ']';
            }

            return $this->response->errorWrongArgs($msg);
        }
        
        $obj = new \stdClass();
        $obj->job_id = $job_id;
        $obj->candidate_ids = Input::get('candidate_ids');
        $obj->message = 'Offer letters are sent to selected candidates';

        return $this->response->withItem($obj, new OfferLetterResultTransformer());
    }
}
