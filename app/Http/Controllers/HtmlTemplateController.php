<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\HtmlTemplateTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\HtmlTemplate\HtmlTemplateRepository;
use Input;


class HtmlTemplateController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, HtmlTemplateRepository $repository)
    {
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new HtmlTemplateTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        return $this->response->withItem($entity, new HtmlTemplateTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $post = Input::all();
            $post['user_id'] = $this->getCurrentUser()->id;
            
            if ($model = $this->repository->create($post)) {
                //return $this->response->withItem($model, new LoanTransformer());
                return $this->response->withItem($model, new HtmlTemplateTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new HtmlTemplateTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }
    
    public function upload()
    {
        $input = Input::all();
        
        $entity = $this->repository->upload($input);
        
        if ($entity) {
            return $this->response->withItem($entity, new HtmlTemplateTransformer());
        }
        
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }
}
