<?php

namespace Fox\Http\Controllers;

use Fox\Exceptions\JobNotFoundException;
use Fox\Repositories\Job\JobRepository;
use Fox\Repositories\Survey\SurveyRepository;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Transformer\Survey\SurveyTransformer;
use Input;


class SurveyController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword', 'job_id', 'company_id']
    ];

    protected $repository;
    protected $jobRepository;

    public function __construct(Response $response,
                                SurveyRepository $repository,
                                JobRepository $jobRepository
    )
    {
        parent::__construct($response);
        $this->repository = $repository;
        $this->jobRepository = $jobRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($jobId)
    {
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $searchFields['job_id'] = $jobId;

        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new SurveyTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($jobId, $id)
    {
        $entity = $this->repository->findForJob($jobId, $id);
        if (!$entity) {
            return $this->response->errorNotFound('Survey could not found');
        }
        return $this->response->withItem($entity, new SurveyTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($jobId)
    {
        try {
            $user = $this->getCurrentUser();
            $job = $this->jobRepository->find($jobId);
            if (!$job) {
                throw new JobNotFoundException();
            }

            $post = [];
            $post['title'] = Input::get('title');
            $post['description'] = Input::get('description');
            $post['user_id'] = $user->id;
            $post['company_id'] = $user->company_id;
            $post['job_id'] = $jobId;

            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new SurveyTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if ($errors) {
                $msg = $msg . ' [' . $errors->first() . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($jobId, $id)
    {
        $entity = $this->repository->findForJob($jobId, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Survey could not be found');
        }

        $post = [];
        $post['title'] = Input::get('title');
        $post['description'] = Input::get('description');

        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new SurveyTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($jobId, $id)
    {
        $entity = $this->repository->findForJob($jobId, $id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Survey could not be found');
        }
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }
}
