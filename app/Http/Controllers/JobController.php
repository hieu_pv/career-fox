<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\JobTransformer;
use Fox\Transformer\JobDetailTransformer;
use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\Job\JobRepository;
use Fox\Repositories\JobQuestion\JobQuestionRepository;
use Fox\Api\Rivs\Video\Crawler;
use Fox\Api\Rivs\Resource\Requisitions;
use Fox\Api\Rivs\Client;
use Fox\JobQuestion;

use Input;
use Validator;
use Config;

class JobController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword','status']
    ];

    protected $repository;
    protected $jobQuestionRepository;

    public function __construct(Response $response, JobRepository $repository, JobQuestionRepository $jobQuestionRepository)
    {
        //$this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
		$this->jobQuestionRepository = $jobQuestionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
			
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        $items = $this->repository->all($searchFields, $searchOptions);
        return $this->response->withPaginator($items, new JobTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        //var_dump($entity); die;
        
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        return $this->response->withItem($entity, new JobDetailTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        global $user;
        
        try {
            $post = Input::all();
            $post['user_id'] = $user->id;
            
            if ( $this->isCompanyGroup() ) {
                $post['company_id'] = $user->company_id;
            }
        
            $post['status'] = 0;
            $post['priority'] = isset($post['priority']) && $post['priority'] == 'urgent' ? 10 : 5;
            
            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new JobTransformer());
            }
            
            $errors = $this->repository->errors();
            
            $msg = 'Input data was wrong';
            if ($errors) {
                if (is_object($errors)) {
                    $msg = $msg . ' [' . $errors->first() . ']';
                } else {
                    $msg = $msg . ' [' . $errors . ']';
                }
            }
            
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        global $user;
        
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $post = Input::all();
        $post['user_id'] = $user->id;
        $post['priority'] = isset($post['priority']) && $post['priority'] == 'urgent' ? 10 : 5;
            
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new JobTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if ($errors) {
            $msg = $msg . ' [' . $errors->first() . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        
        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
        
        $this->repository->delete($id);
        return $this->response->withArray(['id' => $id]);
    }
    
    /**
     * active Job
     */
    public function activeJob()
    {
        global $user;
        $input = Input::all();
        
        $validator = Validator::make($input, ['job_id' => 'required|integer|exists:jobs,id']);
            
        if ($validator->fails()) {
            $msg = 'Input data was wrong';
            $msg = $msg . ' [' . $validator->errors()->first() . ']';
            return $this->response->errorWrongArgs($msg);
        }
        
        $job_id = (int) $input['job_id'];

        // check edit own permission
        if ( $this->isCompanyGroup() && ! $this->companyHasJob($user->company_id, $job_id) ) {
            return $this->response->errorForbidden('You have not permission on this job');
        }
		
		
        $entity = $this->repository->find($job_id);
		
		// Create 	requisitions for this Job
		if(empty($entity->requisition_id))
		{
			$rivsApiConfig = Config::get('rivs.api');
			
			$rivsApiConfig['token'] = str_replace(' OAuth ', '', $rivsApiConfig['token']);
			$client = new Client($rivsApiConfig);
			
			$Requisitions = new Requisitions($client);
			
			$data = [
				"sNameExternal" => $entity->title,
				"sNameInternal" => $entity->title,
				"iBranding" => 23,
				"iStatus" => 1
			];
			$response = $Requisitions->create($data);
			$entity->requisition_id =	$response->body->aaRequisition->iId;
		}
        $entity->status = 1;
        $entity->save();
		
		// Add questions to Riv's
		 $this->authenticateRivs($entity->requisition_id, $entity);
		 
        return $this->response->withArray(['id' => $job_id]);
    }
	
	protected function authenticateRivs($requisition_id, $job)
	{
		$tmp_fname = $_SERVER['DOCUMENT_ROOT'].'cookie.txt';
		
		$rivsConfig = Config::get('rivs.dashboard');

		$options = array('username'=>$rivsConfig['account']['username'],'password'=>$rivsConfig['account']['password'],'cookieFilePath'=>$tmp_fname);
		$questions = $this->repository->findByJobId($job->id);
		$i = 0;
		$questions_ids = [];
		foreach($questions as $question)
		{
			if(empty($question->rivs_question_id))
			{
				$arr['aPrequalifyingQuestion']['n'.$i]['sText'] = $question->title;
				$arr['aPrequalifyingQuestion']['n'.$i]['sType'] = 'radio';
				$arr['aPrequalifyingQuestion']['n'.$i]['aaOptions'][0]['sText'] = (!empty($question->default_answer)?$question->default_answer:'Custom');
				$arr['aPrequalifyingQuestion']['n'.$i]['aaOptions'][0]['sQualifyingAction'] = '';
				$i++;
				$questions_ids[] = $question->id;
			}
		}
		/*$data = [
			'sNameExternal'	=> $job->title,
			'sNameInternal'	=>  $job->title,
			'aPrequalifyingQuestion[n0][sText]' => '454454545454',
			'aPrequalifyingQuestion[n0][sType]' => 'radio',
			'aPrequalifyingQuestion[n0][aaOptions][0][sText]' => 'Try Me',
			'aPrequalifyingQuestion[n0][aaOptions][0][sQualifyingAction]' => '',
			'iRequisition' => $requisition_id,
			'sAction'	=> "form.save",
			'iStatus'	=> 1
		];*/
		if($i>0)
		{
			$data = [
				'sNameExternal'	=> $job->title,
				'sNameInternal'	=>  $job->title,
				'iRequisition' => $requisition_id,
				'sAction'	=> "form.save",
				'iStatus'	=> 1
			];
			$data = array_merge($data, $arr);
			$Crawler = new Crawler($options);
			$res = $Crawler->createRequisitionj('https://v3.rivs.com/requisition/'.$requisition_id.'/update/', http_build_query($data));
			$response = json_decode($res);
			$i = 0;
			foreach($questions_ids as $id)
			{
				$jobQuestion = $this->jobQuestionRepository->find($job->id, $id);
				$post = [];
				$post['title'] = $jobQuestion->title;
				$post['job_id'] = $jobQuestion->job_id;
				$post['rivs_question_id'] = $response->aaPrequalifyingQuestions->aRenames->{'n'.$i};
				$jobQuestionRes = $this->jobQuestionRepository->update($jobQuestion, $post);
				$i++;
			}
		}
		
	}		
}
