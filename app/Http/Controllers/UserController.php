<?php

namespace Fox\Http\Controllers;

use Fox\Transformer\UserTransformer;
use Fox\Transformer\CandidateTransformer;

use EllipseSynergie\ApiResponse\Contracts\Response;
use Fox\Repositories\User\UserRepository;
use Input;

use Fox\User;
use Fox\Company;

class UserController extends ApiController
{

    protected $allowedFields = [
        'search' => ['keyword']
    ];

    protected $repository;

    public function __construct(Response $response, UserRepository $repository)
    {
        $this->loadAndAuthorizeResource();
        
        parent::__construct($response);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        global $user;
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        
        $option = false;
        if ($this->isCompanyGroup()) {
            $option = [
                'company_id' => $user->company_id,
                'user_id' => $user->id
            ];
        }
        $items = $this->repository->all($searchFields, $searchOptions, $option);
        
        return $this->response->withPaginator($items, new UserTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorNotFound('Could not found the given Object');
        }
        return $this->response->withItem($entity, new UserTransformer());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $post = Input::all();
            
            if ($model = $this->repository->create($post)) {
                return $this->response->withItem($model, new UserTransformer());
            }
            $errors = $this->repository->errors();
            $msg = 'Input data was wrong';
            if (is_object($errors)) {
                $msg = $msg . ' [' . $errors->first() . ']';
            } else if ($errors) {
                $msg = $msg . ' [' . $errors . ']';
            }
            return $this->response->errorWrongArgs($msg);
        } catch (\Exception $e) {
            return $this->response->errorWrongArgs($e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $post = Input::all();
        if ($entity = $this->repository->update($entity, $post)) {
            return $this->response->withItem($entity, new UserTransformer());
        }
        $errors = $this->repository->errors();
        $msg = 'Input data was wrong';
        if (is_object($errors)) {
            $msg = $msg . ' [' . $errors->first() . ']';
        } else if ($errors) {
            $msg = $msg . ' [' . $errors . ']';
        }
        return $this->response->errorWrongArgs($msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            return $this->response->errorWrongArgs('The Object Id could not be found');
        }
        $this->repository->delete($entity, $id);
        
        $errors = $this->repository->errors();
        
        if ($errors) {
            return $this->response->errorForbidden($errors);
        }
        return $this->response->withArray(['id' => $id]);
    }
    
    public function getEmployees()
    {
        global $user;
        $companyId = $user->company_id;
        
        $entity = Company::find($companyId);
        if (!$entity) {
            return $this->response->errorWrongArgs('Company not found');
        }
        
        $searchOptions = $this->createQueryOptionsFromRequest();
        $searchFields = $this->searchFields;
        
        $option = [
            'company_id' => $companyId,
            'user_id' => $user->id
        ];
        
        $items = $this->repository->all($searchFields, $searchOptions, $option);
        
        $user_transformer = new UserTransformer();
        $user_transformer->defaultIncludes = ['company'];
        return $this->response->withPaginator($items, new UserTransformer());
    }
    
    public function getUserDetail() {
        global $user;
        
        switch ($user->role) {
            case '1':
                $entity = $user->candidate;
                return $this->response->withItem($entity, new CandidateTransformer());
                break;
            default:
                return $this->response->withItem($user, new UserTransformer());
                break;
        }
        
        
    }
}
