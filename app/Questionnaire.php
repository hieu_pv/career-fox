<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $table = 'questionnaires';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'job_id', 'candidate_id', 'video_interview_id', 'user_id', 'is_invited'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
}
