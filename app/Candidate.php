<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

use \DB;
use \Hash;

class Candidate extends Model implements ICandidate
{
    protected $table = 'candidates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'job_title', 'photo', 'email', 'name', 'resume', 'cover_letter'];

    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];

    public function createCandidate($item)
    {
        $city_id = (int) $item['city_id'];
        
        $city_obj = DB::table('cities')
                ->join('provinces', 'provinces.id', '=', 'cities.province_id')
                ->join('countries', 'countries.id', '=', 'provinces.country_id')
                ->select('provinces.name AS province', 'cities.name AS city', 'countries.name AS country')
                ->where('cities.id', $city_id)
                ->first();

        //var_dump($item); die;

        DB::beginTransaction();

        $user = User::create([
            'name' => isset($item['name']) ? $item['name'] : '',
            'first_name' => $item['first_name'],
            'last_name' => $item['last_name'],
            'password' => Hash::make($item['password']),
            'email' => $item['email'],
            'phone' => isset($item['phone']) ? $item['phone'] : '',
            'role_id' => 11
        ]);

        if (!$user) {
            DB::rollback();
            return false;
        }

        // create candidate
        $candidate_item = [
            'user_id' => $user->id,
            'name' => $item['first_name'] . ' ' . $item['last_name'],
            'email' => $item['email'],
            'job_title' => isset($item['job_title']) ? $item['job_title'] : '',
            'photo' => isset($item['photo']) ? $item['photo'] : '',
            'cover_letter' => isset($item['cover_letter']) ? $item['cover_letter'] : '',
            'resume' => isset($item['resume']) ? $item['resume'] : '',
        ];

        if (isset($item['created_at'])) {
            $candidate_item['created_at'] = $item['created_at'];
        }

        $candidate = Candidate::create($candidate_item);

        // store candidate's address
        $address = Address::create([
            'address' => $item['address'],
            'city' => $city_obj->city,
            'province' => $city_obj->province,
            'country' => $city_obj->country,
            'city_id' => $city_id,
            'phone' => $item['phone'],
            'user_id' => $user->id
        ]); // 'address', 'city', 'province', 'phone', 'user_id'

        if (!$candidate || !$address) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return $candidate;
    }


    /**
     * check if candidate has already applied for a job
     */
    public static function isCandidateInJob($job_id, $candidate_id)
    {
        return Candidate::join('candidate_jobs', 'candidates.user_id', '=', 'candidate_jobs.user_id')
            ->where('candidates.id', $candidate_id)
            ->where('candidate_jobs.job_id', $job_id)
            ->count();
    }

    public function PhoneScreen()
    {
        return $this->hasMany('Fox\PhoneScreen');
    }

    public function VideoInterview()
    {
        return $this->hasMany('Fox\VideoInterview');
    }

    public function companies()
    {
        return $this->belongsToMany('Fox\Company', 'candidate_jobs', 'user_id', 'company_id');
    }

    public function user()
    {
        return $this->belongsTo('Fox\User');
    }

    public function address()
    {
        return $this->hasOne('Fox\Address', 'user_id', 'user_id');
    }

    public function createCandidateFromFrontend($item)
    {
        DB::beginTransaction();

        $user = User::create([
            'name' => isset($item['name']) ? $item['name'] : '',
            'first_name' => $item['first_name'],
            'last_name' => $item['last_name'],
            'password' => Hash::make($item['password']),
            'email' => $item['email'],
            'phone' => isset($item['phone']) ? $item['phone'] : '',
            'role_id' => 11
        ]);

        if (!$user) {
            DB::rollback();
            return false;
        }

        // create candidate
        $candidate_item = [
            'user_id' => $user->id,
            'name' => $item['first_name'] . ' ' . $item['last_name'],
            'email' => $item['email'],
            'photo' => isset($item['photo']) ? $item['photo'] : '',
            'cover_letter' => isset($item['cover_letter']) ? $item['cover_letter'] : '',
            'resume' => isset($item['resume']) ? $item['resume'] : '',
        ];

        if (isset($item['created_at'])) {
            $candidate_item['created_at'] = $item['created_at'];
        }

        $candidate = Candidate::create($candidate_item);

        if (!$candidate) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return $candidate;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->user->first_name;
    }

    public function getLastName()
    {
        return $this->user->last_name;
    }

    public function getEmail()
    {
        return $this->user->email;
    }
}
