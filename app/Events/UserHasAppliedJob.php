<?php

namespace Fox\Events;

use Fox\Events\Event;
use Fox\Job;
use Fox\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserHasAppliedJob extends Event
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;
    /**
     * @var Job
     */
    public $job;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Job $job)
    {
        $this->user = $user;
        $this->job = $job;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
