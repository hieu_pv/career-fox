<?php
/**
 * Created by PhpStorm.
 * User: quynh
 * Date: 5/30/2015
 * Time: 12:12 PM
 */

namespace Fox\Exceptions;


class NotAllowedSearchFieldsException extends \Exception
{
    protected $message = 'The fields you specified cannot be searched';
}