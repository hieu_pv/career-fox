<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class VideoInterview extends Model
{
    protected $table = 'video_interviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_id', 'user_id', 'candidate_id' ,'phone_screen_id', 'quick_note'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function job() {
        return $this->belongsTo('Fox\Job');
    }
    
    public function candidate() {
        return $this->belongsTo('Fox\Candidate');
    }
}
