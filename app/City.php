<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'province_id'];

    public function jobs() {
        return $this->hasMany('Fox\Job');
    }

    public function province() {
        return $this->belongsTo('Fox\Province');
    }
}
