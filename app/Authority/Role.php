<?php
namespace Fox\Authority;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'id'];
    
    
    public function users()
    {
        return $this->hasMany('Fox\User');
    }

}