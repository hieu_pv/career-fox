<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class Job extends Model implements IJob
{
    const STATUS_DELETED = -1;
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;
    const STATUS_FILLED = 3;
    const STATUS_ARCHIVED = 4;
    const STATUS_REOPENED = 5;

    protected $table = 'jobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'city_id',
        'province_id',
        'country_id',
        'city_name',
        'province_name',
        'country_name',
        'postal',
        'priority',
        'description',
        'category_id',
        'company_id',
        'user_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];

    /**
     * Filter only jobs from a company
     */
    public function scopeCompany($query, $company_id)
    {
        return $query->where('jobs.company_id', $company_id);
    }

    public function company()
    {
        return $this->belongsTo('Fox\Company');
    }

    public function category()
    {
        return $this->belongsTo('Fox\Category');
    }

    public function user()
    {
        return $this->belongsTo('Fox\User');
    }

    public function video_interviews()
    {
        return $this->hasMany('Fox\VideoInterview');
    }

    public function interview_notes()
    {
        return $this->hasMany('Fox\InterviewNote');
    }

    public function phone_screens()
    {
        return $this->hasMany('Fox\PhoneScreen');
    }

    public function candidates()
    {
        return $this->belongsToMany('Fox\Candidate', 'candidate_jobs', 'job_id', 'job_id');
    }

    public function country()
    {
        return $this->belongsTo('Fox\Country');
    }

    public function province()
    {
        return $this->belongsTo('Fox\Province');
    }

    public function city()
    {
        return $this->belongsTo('Fox\City');
    }

    public function isDraft()
    {
        return $this->status == self::STATUS_DRAFT;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCity()
    {
        return $this->city_name;
    }
}
