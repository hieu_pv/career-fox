<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class HtmlTemplate extends Model
{
    //
    protected $table = 'html_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'user_id', 'attachment_name', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
