<?php

namespace Fox\Listeners;

use Fox\Events\CandidateWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Fox\APi\MaxHire\Resource\Person;

class MaxHireAddPerson implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CandidateWasCreated $event
     * @return void
     */
    public function handle(CandidateWasCreated $event)
    {
        $candidate = $event->candidate;
        $resource = new Person();
        $result = $resource->add($candidate);
        if ($result->Success) {
            // @todo: Tracking ID of MaxHire for update later
            $candidate->maxhire_person_id = $result->Id;
            $candidate->save();
        }
    }
}
