<?php

namespace Fox\Listeners;

use Fox\Events\JobWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MaxHireDeleteJob implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  JobWasDeleted  $event
     * @return void
     */
    public function handle(JobWasDeleted $event)
    {
        //
    }
}
