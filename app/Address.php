<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address', 'city', 'province', 'country', 'phone', 'user_id', 'city_id', 'company_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function candidate() {
        return $this->belongsTo('Fox\Candidate', 'user_id', 'user_id');
    }
}
