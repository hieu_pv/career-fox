<?php

namespace Fox;

use Illuminate\Database\Eloquent\Model;

use DB;
use Hash;

class Company extends Model
{
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'description', 'phone', 'primary_contact'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function jobs() {
        return $this->hasMany('Fox\Job');
    }
    
    public function candidates() {
        return $this->hasMany('Fox\Candidate');
    }
    
    public function user()
    {
        return $this->hasOne('Fox\User', 'email', 'email');
    }
    
    public function createCompany($item)
    {
        DB::beginTransaction();
        
        // create candidate
        $company_item = [
            'name' => $item['name'],
            'email' => $item['email'],
            'primary_contact' => isset($item['primary_contact']) ? $item['primary_contact'] : '',
            'phone' => isset($item['phone']) ? $item['phone'] : '',
            'description' => isset($item['description']) ? $item['description'] : '',
        ];
        
        if (isset($item['created_at'])) {
            $candidate_item['created_at'] = $item['created_at'];
        }
        
        $company = Company::create($company_item);

        if ( ! $company) {
            DB::rollback();
            return false;
        }
        
        
        $user = User::create([
            'name' => isset($item['name']) ? $item['name'] : '',
            'first_name' => $item['first_name'],
            'last_name' => $item['last_name'],
            'password' => Hash::make($item['password']),
            'email' => $item['email'],
            'phone' => isset($item['phone']) ? $item['phone'] : '',
            'role_id' => 5,
            'company_id' => $company->id
        ]);
        
        if ( ! $user) {
            DB::rollback();
            return false;
        }
        
        DB::commit();
        return $company;
    }
}
