<?php

use Illuminate\Database\Seeder;
use Fox\Authority\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Model::unguard();

        DB::table('roles')->delete();

        $roles = array(
            ['id' => 1, 'name' => 'admin'],
            ['id' => 3, 'name' => 'manager'],
            ['id' => 5, 'name' => 'company'],
            ['id' => 7, 'name' => 'recruiter'],
            ['id' => 9, 'name' => 'employee'],
            ['id' => 11, 'name' => 'candidate']
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($roles as $role)
        {
            Role::create($role);
        }

//        Model::reguard();
    }
}
