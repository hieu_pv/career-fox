<?php

use Illuminate\Database\Seeder;
use Fox\Company;
use Fox\User;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
    
        DB::table('companies')->delete();

        $faker = Faker\Factory::create();
        
        for ($i = 0; $i < 100; $i++) {
            $item = [
                'name' => $faker->unique()->company,
                'email' => 'company' . ($i + 1) . '@gmail.com',
                'phone' => $faker->unique()->PhoneNumber,
                'description' => $faker->text,
                'primary_contact' => $faker->address,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'password' => 'company' . ($i + 1),
            ];
            
            if ($i == 0) {
                $item['email'] = 'onekastudiosinc@gmail.com';
            }
            
            $company = new Company;
            $company->createCompany($item);
        }
    }
}
