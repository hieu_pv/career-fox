<?php

use Illuminate\Database\Seeder;
use Fox\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Model::unguard();

        DB::table('users')->delete();

        $users = array(
            ['first_name' => 'Admin 1', 'last_name' => 'Nguyen', 'email' => 'admin1@gmail.com', 'password' => Hash::make('admin1'), 'role_id' => 1],
            ['first_name' => 'Admin 2', 'last_name' => 'Nguyen', 'email' => 'admin2@gmail.com', 'password' => Hash::make('admin2'), 'role_id' => 1],
            ['first_name' => 'Manager 1', 'last_name' => 'Nguyen', 'email' => 'manager1@gmail.com', 'password' => Hash::make('manager1'), 'role_id' => 3],
            ['first_name' => 'Manager 2', 'last_name' => 'Nguyen', 'email' => 'manager2@gmail.com', 'password' => Hash::make('manager2'), 'role_id' => 3],
            //['first_name' => 'Charles', 'last_name' => 'Oneka', 'email' => 'onekastudiosinc@gmail.com', 'password' => Hash::make('company1'), 'role_id' => 5, 'company_id' => 1],
            //['first_name' => 'Company 2', 'last_name' => 'Oneka', 'email' => 'company2@gmail.com', 'password' => Hash::make('company2'), 'role_id' => 5, 'company_id' => 2],
            ['first_name' => 'Recruiter 1', 'last_name' => 'Nguyen', 'phone' => '0909090909', 'email' => 'recruiter1@gmail.com', 'password' => Hash::make('recruiter1'), 'role_id' => 7, 'company_id' => 1],
            ['first_name' => 'Recruiter 2', 'last_name' => 'Nguyen', 'phone' => '909098887', 'email' => 'recruiter2@gmail.com', 'password' => Hash::make('recruiter2'), 'role_id' => 7, 'company_id' => 2],
            ['first_name' => 'Employee 1', 'last_name' => 'Nguyen', 'phone' => '0909090909', 'email' => 'employee1@gmail.com', 'password' => Hash::make('employee1'), 'role_id' => 9, 'company_id' => 1],
            ['first_name' => 'Employee 2', 'last_name' => 'Nguyen', 'phone' => '909098887', 'email' => 'employee2@gmail.com', 'password' => Hash::make('employee2'), 'role_id' => 9, 'company_id' => 2],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

//        Model::reguard();
    }
}
