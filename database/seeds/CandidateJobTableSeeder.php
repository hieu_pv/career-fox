<?php

use Illuminate\Database\Seeder;
use Fox\CandidateJob;

class CandidateJobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        DB::table('candidate_jobs')->delete();
        
        $jobs_arr1 = DB::table('jobs')->select('id', 'company_id')->where('company_id', 1)->take(10)->get();
        $jobs_arr2 = DB::table('jobs')->select('id', 'company_id')->where('company_id', 2)->take(10)->get();
        $jobs_arr = array_merge($jobs_arr1, $jobs_arr2);
        
        $j = 0;
        foreach ($jobs_arr AS $k => $job) {
            $k = rand(1, 50);
            
            for ($i = 0; $i < $k; $i++) {
                $j++;
                if ($j > 500) {
                    break;
                }
                
                $item = [
                    'user_id' => $j + 4,
                    'company_id' => $job->company_id,
                    'job_id' => $job->id
                ];

                DB::table('candidate_jobs')->insert($item);
            }
        }
    }
}
