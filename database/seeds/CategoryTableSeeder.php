<?php

use Illuminate\Database\Seeder;
use Fox\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
    
        DB::table('categories')->delete();

        $categories = [
            'Accounting/Finance',
            'Admin/Secretarial',
            'Advertising',
            'Architect/Design',
            'Art/Media/Writers',
            'Automotive',
            'Banking',
            'Biotech/Pharmaceutical',
            'Computer/Software',
            'Construction/Skilled Trade',
            'Customer Service',
            'Domestic Help/Care',
            'Education',
            'Engineering',
            'Environmental Science',
            'Events',
            'Everything Else',
            'Facilities/Maintenance',
            'General Labor/Warehouse',
            'Gov/Military',
            'HR & Recruiting',
            'Healthcare',
            'Hospitality/Restaurant',
            'Information Technology',
            'Insurance',
            'Internet',
            'Law Enforcement/Security',
            'Legal',
            'Management & Exec',
            'Manufacturing/Operations',
            'Marketing/PR',
            'Nonprofit & Fund',
            'Oil/Energy/Power',
            'Quality Assurance',
            'Real Estate',
            'Research & Dev',
            'Retail',
            'Sales & Biz Dev',
            'Salon/Beauty/Fitness',
            'Social Services',
            'Supply Chain/Logistics',
            'Telecommunications',
            'Travel',
            'Trucking/Transport',
            'TV/Film/Musicians',
            'Vet & Animal Care',
            'Work from Home',
        ];
        
        //$faker = Faker\Factory::create();
        
        foreach ($categories AS $category) {
            $item = [
                //'title' => $faker->unique()->sentence(3),
                'title' => $category
            ];
            
            Category::create($item);
        }
    }
}
