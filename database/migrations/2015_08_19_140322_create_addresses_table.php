<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('address');
            $table->string('phone', 20);
            $table->string('primary_contact');

            $table->string('city', 50);
            $table->string('province', 50);
            $table->string('country', 50);

            $table->integer('company_id')->unsigned()->default(0);
            
            $table->integer('city_id')->unsigned()->default(0);
            $table->foreign('city_id')->references('id')->on('cities');
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
