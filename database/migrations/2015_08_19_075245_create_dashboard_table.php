<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDashboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jobs', 50);
            $table->string('candidates', 50);
            $table->integer('total_users')->unsigned();
            $table->string('parameters');
            $table->string('revenues');
            $table->string('clients');
            $table->tinyInteger('average_success_rate')->unsigned();
            $table->string('city_job_statistics');
            $table->string('daily_usage');
            $table->integer('average_wage')->unsigned();
            
            // set foreign key to users table
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dashboard');
    }
}
