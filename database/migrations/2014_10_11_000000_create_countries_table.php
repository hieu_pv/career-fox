<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('code', 5);
        });
        
        Schema::create('provinces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('code', 5);
            
            $table->Integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('countries');
        });
        
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('code', 5);
            
            $table->integer('province_id')->unsigned();
            $table->foreign('province_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cities');
        Schema::drop('provinces');
        Schema::drop('countries');
    }
}
