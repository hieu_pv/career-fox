<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyQuestionOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_question_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('survey_question_id')->unsigned();
            $table->foreign('survey_question_id')
                ->references('id')
                ->on('survey_questions');
            $table->string('title');
            $table->string('value');
            $table->boolean('is_answer')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_question_options');
    }
}
