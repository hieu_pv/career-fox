<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            //$table->string('username', 50);
            $table->string('email', 100)->unique();
            $table->string('password', 60);
            $table->string('phone', 30);
            
            $table->tinyInteger('role_id');
            
            $table->integer('company_id')->unsigned()->default(0);
            $table->rememberToken();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
