<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('company_name', 50);
            $table->string('primary_contact');
            $table->string('name', 50);
            
            // set foreign key to users table
            //$table->integer('user_id')->unsigned();
            //$table->foreign('user_id')->references('id')->on('users');
            
            $table->string('phone', 30);
            $table->string('bio'); // what is this field?
            $table->string('email', 100);
            //$table->string('address');
            $table->text('description');
            $table->string('employee_name', 50);
            $table->string('employee_position'); // maybe a integer, meed to be reviewed
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
