@extends('layouts.master')

@section('title', 'Job Listing')

@section('content')
<div class="container-fluid container-fullw ng-scope">
    <div class="row">
        <div class="col-md-12">
            <h1 class="mainTitle text-primary">Job listing</h1>

            @if (count($jobs))
            <table class="table table-striped table-user">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Location</th>
                        <th>Date Posted</th>
                        <th>Candidates</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($jobs AS $k => $job)
                        <?php
                            $status = 'inactive';
                            if ($job->status === 0) {
                                $status = 'draft';
                            } else if ($job->status == 1) {
                                $status = 'active';
                            }
                            $time_ago = time() - strtotime($job->created_at);
                            $days_ago = ceil($time_ago/(24 * 3600));
                            
                            $location = "{$job->city_name},  $job->province_name, $job->country_name"; 
                        ?>
                        <tr>
                            <td>
                                {{++$k}}
                            </td>
                            <td>{{$job->title}}</td>
                            <td>{{$location}}</td>
                            <td>{{$days_ago}}</td>
                            <td>{{count($job->candidates)}}</td>
                            <td>
                                <a href="{{route('demo.job.id', [$job->id])}}">Apply</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            <div class="text-right">
                {!! $jobs->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection