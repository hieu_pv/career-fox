@extends('layouts.master')

@section('title', 'Apply Job')

@section('content')
<div class="container-fluid container-fullw ng-scope">
    <div class="row">
        <div class='col-md-12'>
            <p></p>
            <p>If you are an existing user, enter your email address and password to log in. If you have forgotten your password, you can retrieve your password via email using the link provided below. If you are a new user, please provide the information requested to register now.</p>
        </div>
        <div class="col-md-6">
            <h3>New User Registration</h3>
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() AS $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
            @endif
            {!! Form::open(['files' => true]) !!}
            <div class="form-group">
                {!! Form::label('first_name', 'First Name') !!}
                {!! Form::text('first_name', Input::old('first_name'), ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('last_name', 'Last Name') !!}
                {!! Form::text('last_name', Input::old('last_name'), ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::email('email', Input::old('email'), ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', 'Password') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('phone', 'Phone') !!}
                {!! Form::text('phone', Input::old('phone'), ['class' => 'form-control']) !!}
            </div>
            
            <div class="form-group">
                {!! Form::label('resume', 'Resume') !!}
                {!! Form::file('resume') !!}
            </div>
            <div class="form-group">
                {!! Form::label('cover_letter', 'Cover Letter') !!}
                {!! Form::file('cover_letter') !!}
            </div>
            
            
            <div class="form-group">
                {!! Form::label('filetype', 'File types for resume and CV') !!}
                <div>pdf, doc, docx</div>
            </div>
            <div class="form-group">
                {!! Form::submit('Register', ['class' => 'form-control btn btn-primary']) !!}
            </div>
            
            {!! Form::close() !!}
        </div>
        
        <div class="col-md-6">
            <h3>Existing User Login</h3>
            @if (session('message'))
            <ul class="alert alert-danger">
                <li>{{ session('message') }}</li>
            </ul>
            @endif
            
            <form method="post" action="{{url('demo/login')}}">
                <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password') !!}
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Login', ['class' => 'form-control btn btn-primary']) !!}
                </div>
                
                <input type='hidden' value='{{$job_id}}' name="job_id" />
            
            </form>
        </div>
    </div>
</div>
<!-- 
<script>
    (function($){
        var public_url = '{{url("demo")}}';
        
        $('#country').on('change', function() {
            var country_id = parseInt($(this).val());
            $('#province, #city').attr('disabled', 'disabled').html('');
            
            if (country_id) {
                var province_url = public_url + '/getProvinces/' + country_id;
                
                $.getJSON(province_url, function(items){
                    var provinces_str = '<option value="0" selected>Select</option>';
                    for (var i = 0; i < items.length; i++) {
                        provinces_str += '<option value="' + items[i].id + '">' + items[i].name + '</option>';
                    }
                    
                    $('#province').removeAttr('disabled').html(provinces_str);
                });
            }
        });
        
        $('#province').on('change', function() {
            var province_id = parseInt($(this).val());
            $('#city').attr('disabled', 'disabled').html('');
            
            if (province_id) {
                var city_url = public_url + '/getCities/' + province_id;
                
                $.getJSON(city_url, function(items){
                    var cities_str = '<option value="0" selected>Select</option>';
                    for (var i = 0; i < items.length; i++) {
                        cities_str += '<option value="' + items[i].id + '">' + items[i].name + '</option>';
                    }
                    
                    $('#city').removeAttr('disabled').html(cities_str);
                });
            }
        });
    })(jQuery);
</script> -->
@endsection