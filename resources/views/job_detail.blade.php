@extends('layouts.master')

@section('title', 'Apply Job')

@section('content')
<div class="container-fluid container-fullw ng-scope">
    <div class="row">
        <div class="col-md-12">
            <h1 class="mainTitle text-primary">Apply Job</h1>
            <h3>{{$job->title}}</h3>
            @if (session('message')) 
            <div class='alert alert-success'>{{session('message')}}</div>
            @endif
            <p><b>Location</b>: {{$job->city_name}} , {{$job->province_name}}, {{$job->country_name}}</p>
            <p><b>Category</b>: {{$job->category->title}}</p>
            <p><b>Company</b>: {{$job->company->name}}</p>
            <b>Description</b>:
            <div>{!!$job->description!!}</div>
            <p></p>
            <form action="" method='post'><button type='submit' class="btn btn-primary">Apply Now</button></form>
            
        </div>
    </div>
</div>
@endsection