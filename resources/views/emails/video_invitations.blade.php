Hi {{$data->employee_fname}},

<p>A new candidate is invited to Video Interview. Please review info below.</p>
<table>
    <tr>
        <td>Name:</td>
        <td>{{$data->name}}</td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>{{$data->email}}</td>
    </tr>
    <tr>
        <td>Phone:</td>
        <td>{{$data->phone}}</td>
    </tr>
</table>
