var path = require('path');

function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*', {
        cwd: path
    }).forEach(function(option) {
        key = option.replace(/\.js$/, '');
        object[key] = require(path + option);
    });
    return object;
}

module.exports = function(grunt) {
    'use strict';

    // 1. Load configs
    var fileConfig = require('./grunt-tasks/config');
    var taskConfig = {
        pkg: grunt.file.readJSON('package.json'),
        env: process.env || 'development',

        /**
         * The banner is the comment that is placed at the top of our compiled
         * source files. It is first processed as a Grunt template, where the '<%='
         * pairs are evaluated based on this very configuration object.
         */
        meta: {
            banner: '/**\n' +
                ' * <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                ' *\n' +
                ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
                ' */\n'
        }
    };

    grunt.util._.extend(taskConfig, fileConfig);
    grunt.util._.extend(taskConfig, loadConfig('./grunt-tasks/options/'));

    grunt.initConfig(taskConfig);

    // 2. Load grunt task modules
    require('load-grunt-tasks')(grunt);

    // 3. Load tasks
    grunt.loadTasks('grunt-tasks');

    // 4. Default task
    grunt.registerTask('default', ['build']);
};