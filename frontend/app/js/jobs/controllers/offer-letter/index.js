(function (app) {
    app.controller('JobsOfferLetter', controller);
    controller.$inject = [
        '$state',
        'job',
        'API'
    ];

    function controller($state,
                        job,
                        API) {

        var vm = this;
        vm.ckeditorOptions = {
            toolbar: 'basic',
            height: '270px'
        };

        var params = {};

        vm.candidates = [];
        vm.keyword = '';
        // get candidates invited
        function getCandidates() {
            var params = {};
            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }
            API.jobs.offer.list(job.getId(), params)
                .then(function (recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }
                })
                .catch(function (err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
        }

        init();

        vm.selectedCandidate = null;

        vm.select = function (item) {
            vm.selectedCandidate = item;
        };

        vm.isActive = function (item) {
            if (!vm.selectedCandidate) {
                return false;
            }
            return (vm.selectedCandidate.getId() === item.getId());
        };


        vm.gotoReferenCheck = function () {
            $state.go('fox.jobs.reference-check');
        };

        vm.gotoPlacementRecord = function () {
            $state.go('fox.jobs.placement-record');
        };

    }
})(angular.module('fox.jobs.controllers.offer-letter', ['ckeditor']));