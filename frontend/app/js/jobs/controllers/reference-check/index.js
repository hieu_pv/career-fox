(function (app) {
    app.controller('JobsReferenceCheck', controller);
    controller.$inject = [
        '$state',
        'job',
        'API',
        'dialog',
		'ngDialog'
    ];

    function controller($state,
                        job,
                        API,
                        dialog,
						ngDialog) {

        var vm = this;
        vm.ckeditorOptions = {
            toolbar: 'basic',
            height: '395px'
        };

        var params = {};

        vm.candidates = [];
        vm.keyword = '';
        // get candidates invited
        function getCandidates() {
            var params = {};
            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }
            API.jobs.reference.list(job.getId(), params)
                .then(function (recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }
                })
                .catch(function (err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
        }

        init();

        vm.selectedCandidate = null;

        vm.select = function (item) {
            vm.selectedCandidate = item;
        };

        vm.isActive = function (item) {
            if (!vm.selectedCandidate) {
                return false;
            }
            return (vm.selectedCandidate.getId() === item.getId());
        };

        vm.isInvited = function () {
            if (!vm.selectedCandidate) {
                return false;
            }
            return vm.selectedCandidate.isInvitedOffer();
        };

        vm.isSelected = function () {
            if (vm.selectedCandidate) {
                return true;
            }
            return false;
        };

        vm.showInvite = function () {
            if (!vm.isSelected()) {
                return true;
            }

            return !vm.selectedCandidate.isInvitedOffer();
        };

        vm.invite = function () {
            if (!vm.isSelected()) {
                return false;
            }

            var id = vm.selectedCandidate.reference_check_id;

            API.jobs.reference.invite(id)
                .then(function (result) {
                    vm.selectedCandidate.setInvitedOffer(true);
                    dialog.alert(null, 'The candidate has been invited successfully!');
                })
                .catch(function (err) {
                    throw err;
                });
        };


        vm.gotoInterviewNote = function () {
            $state.go('fox.jobs.interview-note');
        };

        vm.gotoOfferLetter = function () {
            $state.go('fox.jobs.offer-letter');
        };
		
		vm.addReference = function(){
			showDlg();
		};
		
		function showDlg() {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-users',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
                        $scope.create = function() {
                            if ($scope.reference.getId()) {
                                var data = {
                                    first_name: $scope.reference.name,
									email: $scope.reference.email,
									phone: $scope.reference.phone
                                };
                                API.reference.update($scope.reference.getId(), data)
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            } else {
                                API.reference.create($scope.reference.name, $scope.reference.email, $scope.reference.phone )
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            }
                        };
                    }
                ],
                template: 'jobs/templates/reference-check/add-reference.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }
    }
})(angular.module('fox.jobs.controllers.reference-check', ['ckeditor']));