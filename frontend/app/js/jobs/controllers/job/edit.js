var City = require('../../../api/models/city');
var Province = require('../../../api/models/province');
var Category = require('../../../api/models/category');

(function(app) {
    app.controller('JobsJobEdit', controller);
    controller.$inject = [
        '$state',
        'API',
        'apiUrl',
        'job'
    ];

    function controller(
        $state,
        API,
        apiUrl,
        job
    ) {

        var vm = this;

        vm.job = job;
        vm.city = job.city;
        vm.category = job.category;
        vm.province = job.province;
        vm.city.setProvince(job.province);

        vm.ckeditorOptions = {
            toolbar:'basic',
            height:'500px'
        };


        function update() {
            vm.job.city = vm.city;
            vm.job.province = vm.province;
            vm.job.category = vm.category;
            return API.jobs.update(vm.job);
        }

        vm.saveDraft = function() {
            var promise = update();

            // update
            promise
                .then(function(rs) {
                    vm.job.bind(rs.data);
                }, function(err) {
                    throw err;
                });
        };

        vm.submitForm = function() {
            var promise = update();

            promise
                .then(function(rs) {
                    $state.go('fox.jobs.create-interview', {
                        id: rs.data.id
                    });
                }, function(err) {
                    throw err;
                });
        };

        vm.cancel = function() {
            $state.go('fox.my-jobs.list');
        };

        vm.filterUrls = {
            city: apiUrl.get('cities'),
            province: apiUrl.get('provinces'),
            category: apiUrl.get('categories')
        };

        /**
         * Format Query String before sending
         * for auto complete
         *
         * @param str
         * @returns {{q: string, limit: number, fields: string}}
         */
        vm.cityRequestFormat = function(str) {
            return {
                q: '(keyword:' + str + ')',
                limit: 20,
                include: 'province'
            };
        };

        /**
         * Callback function for auto complete
         * @param object
         */
        vm.citySelectCallback = function(object) {
            if (object && object.originalObject) {
                vm.city = new City(object.originalObject);
                vm.province = vm.city.province;
            } else {
                vm.city.bind({});
                vm.province.bind({});
            }
        };

        vm.categorySelectCallback = function(object) {
            if (object && object.originalObject) {
                vm.category.bind(object.originalObject);
            } else {
                vm.category.bind({});
            }
        };

        /**
         * Format Query String before sending
         * for auto complete
         *
         * @param str
         * @returns {{q: string, limit: number, fields: string}}
         */
        vm.categoryRequestFormat = function(str) {
            return {
                q: '(keyword:' + str + ')',
                limit: 20
            };
        };
    }
})(angular.module('fox.jobs.controllers.job.edit', []));