(function (app) {
    app.controller('JobsPlacementRecord', controller);
    controller.$inject = [
        '$state',
        'job',
        'API',
		'$scope',
		'factoryService',
		'ngDialog',
		'$stateParams'
    ];

    function controller($state,
                        job,
                        API, 
						$scope,
						factoryService,
						ngDialog,
						$stateParams) {

        var vm = this;

        var params = {};

        vm.candidates = [];
        vm.keyword = '';

        // get candidates invited
        function getCandidates() {
            var params = {};
            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }
            API.jobs.offer.list(job.getId(), params)
                .then(function (recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }
                })
                .catch(function (err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
        }

        init();

        vm.selectedCandidate = null;

        vm.select = function (item) {
            vm.selectedCandidate = item;
        };

        vm.isActive = function (item) {
            if (!vm.selectedCandidate) {
                return false;
            }
            return (vm.selectedCandidate.getId() === item.getId());
        };

        vm.gotoOfferLetter = function () {
            $state.go('fox.jobs.offer-letter');
        };
		
		
		// get benefit list
        vm.benefits = [];
        vm.bePaginator = factoryService.createPaginator();

        vm.getBenefitList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.benefitPaginator) {
                currentPage = vm.benefitPaginator.current_page;
                limit = vm.benefitPaginator.per_page;
            }

            var params = {
                job_id: jobId,
                page: currentPage,
                limit: limit
            };

            API.benefit.list(params)
                .then(function(recordSet) {
                    vm.benefits = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };
		
		function showDlg(benefit) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-benefits',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
                        $scope.create = function() {
                            if ($scope.benefit.getId()) {
                                var data = {
                                    title: $scope.benefit.title,
									description: $scope.benefit.description,
									start_date: $scope.benefit.start_date
                                };
                                API.benefit.update($scope.benefit.getId(), data)
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            } else {
                                API.benefit.create($scope.benefit.title, $scope.benefit.description, $scope.benefit.start_date )
                                    .then(function(rs) {
                                        $scope.confirm(true);
                                    }, function(err) {
                                        throw err;
                                    });
                            }
                        };
                    }
                ],
                template: 'placement-record/add.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }

        vm.addBenifits = function() {
			//var benefit = new Benefit({});
			var benefit = {};
			showDlg(benefit);
        };
		
		vm.addNewRecords = function() {
			//var newRecord = new newRecord({});
			var newRecord = {};
			recordShowDlg(newRecord);
        };
		
		function recordShowDlg(newRecord) {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-record',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
						vm =this;
                        vm.items = [];
						vm.paginator = factoryService.createPaginator();
						vm.keyword = '';
						vm.invited = [];

						vm.getList = function() {
							var currentPage = 1;
							var limit = 20;
							if (vm.paginator) {
								currentPage = vm.paginator.current_page;
								limit = vm.paginator.per_page;
							}

							var params = {
								page: currentPage,
								limit: limit
							};

							if (vm.keyword) {
								params.q = ('(keyword:' + vm.keyword + ')');
							}

							var jobId = $stateParams.id || null;
							
							API.jobs.candidates.getReviewList(jobId, params)
								.then(function(recordSet) {
									vm.items = recordSet.getItems();
									vm.items.forEach(function(value, key) {
										if (vm.invited.indexOf(value.id) > -1) {
											vm.items[key].is_invited = true;
										}
									});
									vm.paginator = recordSet.getPaginator();
								})
								.catch(function(err) {
									throw err;
								});
						};

						// init functions for list candidate of a job
						vm.getList();

						vm.keypress = function(e) {
							if (e.which === 13) {
								vm.paginator.current_page = 1;
								vm.getList();
							}
						};

                    }
                ],
                template: 'jobs/templates/candidate/popup-review.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        }
		
		vm.uploadShowDlg = function() {
            var dlg = ngDialog.openConfirm({
                className: 'ngdialog-theme-default add-benefits',
                showClose: false,
                resolve: {
                    
                },
                controller: ['$scope',
                    function($scope) {
                        $scope.create = function() {
                            
                        };
                    }
                ],
                template: 'placement-record/add-documents.tpl.html'
            });
            dlg.then(function(val) {
                vm.benefitPaginator.current_page = 1;
                vm.getBenefitList();
            }, function(reason) {

            });
        };

        vm.uploadDocument = function() {
			vm.uploadShowDlg();
        };
		
    }
})(angular.module('fox.jobs.controllers.placement-record', []));