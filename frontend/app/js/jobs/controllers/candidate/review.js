(function(app) {
    app.controller('JobsCandidateReview', controller);
    controller.$inject = [
        '$state',
        'dialog',
        'job',
        'factoryService',
        'API'
    ];

    function controller(
        $state,
        dialog,
        job,
        factoryService,
        API
    ) {

        var vm = this;

        vm.items = [];
        vm.paginator = factoryService.createPaginator();
        vm.keyword = '';
        vm.invited = [];

        vm.getList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.paginator) {
                currentPage = vm.paginator.current_page;
                limit = vm.paginator.per_page;
            }

            var params = {
                page: currentPage,
                limit: limit
            };

            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }

            API.jobs.candidates.getReviewList(job.getId(), params)
                .then(function(recordSet) {
                    vm.items = recordSet.getItems();
                    vm.items.forEach(function(value, key) {
                        if (vm.invited.indexOf(value.id) > -1) {
                            vm.items[key].is_invited = true;
                        }
                    });
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        // init functions for list candidate of a job
        vm.getList();

        vm.keypress = function(e) {
            if (e.which === 13) {
                vm.paginator.current_page = 1;
                vm.getList();
            }
        };

        vm.gotoPhoneScreen = function() {
            // invite to phone screen
            var ids = [];

            angular.forEach(vm.items, function(item) {

                if (item.hasInvitedToPhone()) {
                    ids.push(item.getId());
                }
            });
            var jobId = job.getId();

            if (ids.length > 0) {
                API.jobs.candidates.invitePhone(jobId, ids)
                    .then(function(rs) {
                        $state.go('fox.jobs.phone-screen');
                    }, function(err) {
                        throw err;
                    });
            } else {
                $state.go('fox.jobs.phone-screen');
            }
        };

        vm.gotoQuestion = function() {
            $state.go('fox.jobs.create-interview');
        };

        vm.getShortContent = function(item) {
            return item.getShortResume();
        };

        vm.getCoverLetter = function(item) {
            return item.getShortLetter();
        };

        vm.openDetail = function(item) {
            dialog.openCandidateDetail(item);
        };

        vm.openCoverLetter = function(item) {
            dialog.openCandidateCoverLetter(item);
        };

        vm.inviteToPhone = function(item) {
            var jobId = job.getId();
            var id = item.getId();
            var promise = null;

            var valResponseErr = false;
            // is checked
            if (item.hasInvitedToPhone()) {
                // invite to phone
                promise = API.jobs.candidates.invitePhone(jobId, id);
            } else {
                valResponseErr = true;
                // remove invite
                promise = API.jobs.candidates.removeInvitePhone(jobId, id);
            }

            promise.then(function(rs) {
                if (item.hasInvitedToPhone()) {
                    dialog.alert(null, 'The candidate has been invited to phone screen');
                } else {
                    dialog.alert(null, 'The candidate has been removed to phone screen');
                }
            }, function(err) {
                item.setInvitedPhone(valResponseErr);
                throw err;
            });
        };

        vm.updateInvited = function(item) {
            if (item.is_invited) {
                vm.invited.push(item.id);
            } else {
                vm.invited.forEach(function(value, key) {
                    if (value == item.id) {
                        vm.invited.splice(key, 1);
                    }
                });
            }
        };
    }
})(angular.module('fox.jobs.controllers.candidate.review', []));
