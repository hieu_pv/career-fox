(function(app) {
    app.controller('JobsReviewVideoInterview', controller);
    controller.$inject = [
        '$state',
        'dialog',
        'ngDialog',
        'job',
        'factoryService',
        'API'
    ];

    function controller(
        $state,
        dialog,
        ngDialog,
        job,
        factoryService,
        API
    ) {

        var vm = this;

        vm.gotoPhoneScreen = function() {
            $state.go('fox.jobs.phone-screen');
        };

        vm.gotoQuestionResult = function() {
            $state.go('fox.jobs.review-question-result');
        };

        vm.candidates = [];
        vm.keyword = '';
        // get candidates invited
        function getCandidates() {
            var params = {};

            if (vm.keyword) {
                params.q = ('(keyword:' + vm.keyword + ')');
            }

            API.jobs.videoInterview.list(job.getId(), params)
                .then(function(recordSet) {
                    vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }
                })
                .catch(function(err) {
                    throw err;
                });
        }

        vm.search = function() {
            getCandidates();
        };

        vm.keypress = function(e) {
            if (e.which === 13) {
                vm.search();
            }
        };

        vm.questions = [];

        function getQuestions() {
            var params = {
                job_id: job.getId()
            };

            API.question.list(params)
                .then(function(recordSet) {
                    vm.questions = recordSet.getItems();
                    // set default data
                    if (vm.questions.length) {
                        vm.selectedQuestion = vm.questions[0];
						vm.getInterviewVideos(vm.selectedCandidate.getId());
                    }
                })
                .catch(function(err) {
                    throw err;
                });
        }

        function init() {
            // get candidates invited
            getCandidates();
            // get questions
            getQuestions();
        }

        init();

        vm.selectedCandidate = null;

        vm.select = function(item) {
            vm.selectedCandidate = item;
        };
		
		vm.getInterviewVideos = function(item) {
		
		   var params = {"job_id": job.id,"candidate_id": item, "getReqCadId":"video"};
		 
					
			API.jobs.videoInterview.list(job.getId(), params)
                .then(function(recordSet) {
				console.log(recordSet);
                   /* vm.candidates = recordSet.getItems();
                    // set default selected
                    if (vm.candidates.length) {
                        vm.selectedCandidate = vm.candidates[0];
                    }*/
                })
                .catch(function(err) {
                    throw err;
                });
		   
        
		};

        vm.isActive = function(item) {
            if (!vm.selectedCandidate) {
                return false;
            }

            return (vm.selectedCandidate.getId() === item.getId());
        };

        vm.selectedQuestion = null;

        vm.selectQuestion = function(item) {
            vm.selectedQuestion = item;
        };
		
		vm.getclicked = function(item,total) {
		
		clicked_td = "question_"+item;
			get_clicked_number = clicked_td.split('_');
			document.getElementById('video_'+get_clicked_number[1]).style.display = "block";
			for(i=1;i<=total;i++){
				if(i != get_clicked_number[1]){
						document.getElementById('video_'+i).style.display = "none";
				}
			}
            
        };

        vm.isQuestionActive = function(item) {
            if (!vm.selectedQuestion) {
                return false;
            }

            return (vm.selectedQuestion.getId() === item.getId());
        };

        vm.getInterviewName = function() {
            if (!vm.selectedCandidate) {
                return '';
            }

            return vm.selectedCandidate.getName();
        };

        vm.saveNote = function() {

        };

        vm.inviteToQuestion = function() {
            if (!vm.isCandidateSelected()) {
                return false;
            }
            API.jobs.videoInterview.invite(vm.selectedCandidate.video_interview_id)
                .then(function(rs) {
                    vm.selectedCandidate.setInvitedQuestionnaire(true);
                    dialog.alert(null, 'The candidate has been invited successfully!');
                }, function(err) {
                    throw err;
                });
        };

        vm.isCandidateSelected = function() {
            if (!vm.selectedCandidate) {
                return false;
            }
            return true;
        };

        vm.showInvite = function() {
            if (!vm.isCandidateSelected()) {
                return true;
            }
            return !vm.selectedCandidate.isInvitedQuestionnaire();
        };

        vm.share = function() {
            if (vm.selectedCandidate) {

                var dlg = ngDialog.open({
                    className: 'ngdialog-theme-default share-candidate',
                    showClose: false,
                    resolve: {
                        candidate: function() {
                            return vm.selectedCandidate;
                        }
                    },
                    controller: [
                        '$scope',
                        'candidate',
                        'factoryService',
                        'API',
                        function(
                            $scope,
                            candidate,
                            factoryService,
                            API
                        ) {
                            $scope.employees = [];
                            $scope.paginator = factoryService.createPaginator();

                            $scope.getList = function() {
                                var currentPage = 1;
                                var limit = 20;
                                if ($scope.paginator) {
                                    currentPage = $scope.paginator.current_page;
                                    limit = $scope.paginator.per_page;
                                }

                                var params = {
                                    page: currentPage,
                                    limit: limit
                                };

                                API.employees.list(params)
                                    .then(function(recordSet) {
                                        $scope.employees = recordSet.getItems();
                                        $scope.paginator = recordSet.getPaginator();
                                    })
                                    .catch(function(err) {
                                        throw err;
                                    });
                            };

                            $scope.share = function() {
                                // call api share

                                var ids = [];
                                angular.forEach($scope.employees, function(item) {
                                    if (item.checked) {
                                        ids.push(item.getId());
                                    }
                                });

                                if (ids.length === 0) {
                                    dialog.alert(null, 'Please select at least one employee!');
                                    return false;
                                }

                                // API.jobs.candidates.share(phoneScreen.getId(), ids)
                                //     .then(function(rs) {
                                //         var alert = dialog.alert('', 'The candidate is shared successfully');
                                //         alert.closePromise
                                //             .then(function() {
                                //                 $scope.closeThisDialog('shared');
                                //             });
                                //     }, function(err) {
                                //         throw err;
                                //     });
                            };

                            $scope.getList();
                        }
                    ],
                    template: 'jobs/templates/phone-screen/share.tpl.html'
                });

            } else {
                dialog.alert(null, 'Please select a candidate.');
            }
        };
    }
})(angular.module('fox.jobs.controllers.review-video-interview', []));