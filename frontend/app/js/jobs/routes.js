require('./controllers/');

(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.jobs', {
                    abstract: true,
                    templateUrl: 'jobs/templates/index.tpl.html',
                    resolve: {
                        job: function($stateParams, API) {
                            var id = $stateParams.id;
                            return API.jobs.get(id);
                        }
                    },
                    controller: 'JobsController',
                    controllerAs: 'vm',
                    url: '/jobs/{id:[0-9]{1,}}'
                })
                .state('fox.jobs-create', {
                    abstract: true,
                    templateUrl: 'jobs/templates/create-menu.tpl.html',
                    resolve: {}
                })
                .state('fox.jobs-create.create', {
                    url: '/jobs/create',
                    templateUrl: 'jobs/templates/job/create.tpl.html',
                    controller: 'JobsJobCreate',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.edit', {
                    url: '/edit',
                    templateUrl: 'jobs/templates/job/create.tpl.html',
                    controller: 'JobsJobEdit',
                    controllerAs: 'vm'
                })
                .state('fox.jobs.create-interview', {
                    url: '/interview-assessment-questionnaire',
                    templateUrl: 'jobs/templates/interview/create.tpl.html',
                    controller: 'JobsCreateInterview',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.candidate-review', {
                    url: '/candidates/review',
                    templateUrl: 'jobs/templates/candidate/review.tpl.html',
                    controller: 'JobsCandidateReview',
                    controllerAs: 'vm'
                })
                .state('fox.jobs.phone-screen', {
                    url: '/phone-screen',
                    templateUrl: 'jobs/templates/phone-screen/index.tpl.html',
                    controller: 'JobsPhoneScreen',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.review-video-interview', {
                    url: '/review-video-interview',
                    templateUrl: 'jobs/templates/review-video-interview/index.tpl.html',
                    controller: 'JobsReviewVideoInterview',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.review-question-result', {
                    url: '/review-question-result',
                    templateUrl: 'jobs/templates/review-question-result/index.tpl.html',
                    controller: 'JobsReviewQuestionResult',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.interview-note', {
                    url: '/interview-note',
                    templateUrl: 'jobs/templates/interview-note/index.tpl.html',
                    controller: 'JobsInterviewNote',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.reference-check', {
                    url: '/reference-check',
                    templateUrl: 'jobs/templates/reference-check/index.tpl.html',
                    controller: 'JobsReferenceCheck',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.offer-letter', {
                    url: '/offer-letter',
                    templateUrl: 'jobs/templates/offer-letter/index.tpl.html',
                    controller: 'JobsOfferLetter',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.jobs.placement-record', {
                    url: '/placement-record',
                    templateUrl: 'jobs/templates/placement-record/index.tpl.html',
                    controller: 'JobsPlacementRecord',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.jobs.routes', [
    'fox.jobs.controllers'
]));