(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.settings.users', {
                    url: '/settings/users',
                    templateUrl: 'users/list.tpl.html',
                    controller: 'UsersController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.users.routes', []));