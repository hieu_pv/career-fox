(function(app) {
    app.controller('UsersController', controller);
    controller.$inject = [
        '$state',
        '$rootScope',
        '$scope',
        'factoryService',
        'API',
        'ngDialog',
        'dialog'
    ];

    function controller(
        $state,
        $rootScope,
        $scope,
        factoryService,
        API,
        ngDialog,
        dialog
    ) {

        var vm = this;
        vm.keyword = '';
        vm.items = [];
        vm.paginator = factoryService.createPaginator();

        vm.addUser = function() {
            ngDialog.openConfirm({
                showClose: false,
                resolve: {

                },
                className: 'ngdialog-theme-default add-user',
                controller: ['$scope', '$rootScope',
                    function($scope, $rootScope) {
                        var roles = [];
                        switch ($rootScope.loggedInUser.role_name) {
                            case 'admin':
                                roles = ['admin', 'manager', 'recruiter', 'employee'];
                                break;
                            case 'company':
                                roles = ['recruiter', 'employee'];
                                break;
                        }
                        $scope.roles = roles;

                        $scope.model = {
                            email: '',
                            first_name: '',
                            last_name: '',
                            phone: '',
                            role: $scope.roles[0],
                            password: '',
                            password_confirmation: ''
                        };
                        if ($rootScope.loggedInUser.role_name == 'company') {
                            $scope.model.company_id = $rootScope.loggedInUser.company_id;
                        }
                        $scope.confirm = function(model) {
                            if (model.password != model.password_confirmation) {
                                dialog.alert("", "Password confirmation does not match.");
                                return;
                            }
                            API.user.create(model)
                                .then(function(result) {
                                    vm.items.push(result);
                                    $scope.closeThisDialog('cancel');
                                });
                        };
                    }
                ],
                template: 'users/add/add.tpl.html'
            });
        };
        
        vm.getList = function() {
            var currentPage = 1;
            var limit = 20;
            if (vm.paginator) {
                currentPage = vm.paginator.current_page;
                limit = vm.paginator.per_page;
            }

            var params = {
                page: currentPage,
                limit: limit
            };

            var arrSearch = [];
            if (vm.keyword) {
                arrSearch.push('keyword:' + vm.keyword);
            }
            params.q = '(' + arrSearch.toString() + ')';

            API.employees.list(params)
                .then(function(recordSet) {
                    vm.items = recordSet.getItems();
                    vm.paginator = recordSet.getPaginator();
                })
                .catch(function(err) {
                    throw err;
                });
        };

        vm.getList();

        vm.search = function() {
            vm.paginator.current_page = 1;
            vm.getList();
        };

        vm.keypress = function(e) {
            if (e.which === 13) {
                vm.search();
            }
        };

        vm.allRows = false;
        vm.toggleRows = function() {
            angular.forEach(vm.items, function(item) {
                item.checked = vm.allRows;
            });
        };
    }
})(angular.module('fox.users.list', []));
