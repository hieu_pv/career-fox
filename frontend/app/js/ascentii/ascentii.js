(function(app) {
    app.controller('AscentiiController', controller);
    controller.$inject = [
        '$state',
        'factoryService',
        'dialog',
        'Ascentii',
        'API'
    ];

    /** usage */
    Ascentii.getOptions();

})(angular.module('fox.ascentii.controller', []));

app.factory('Ascentii', function($q, $http, $log) {

    return {
        getOptions: function() {
            var deferred = $q.defer();
            $http.get('ascentii')
                .success(function(data) {
                    console.log("Ascentii: " + data);
                    deferred.resolve({
                        //get select options
                    });
                }).error(function(msg, code) {
                    deferred.reject(msg);
                    $log.error(msg, code);
                });
            return deferred.promise;
        }
    };
});
