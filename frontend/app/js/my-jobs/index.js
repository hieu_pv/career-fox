require('./my-jobs');
require('./routes');

(function(app) {})(angular.module('fox.my-jobs', [
    'fox.my-jobs.routes',
    'fox.my-jobs.controllers.list'
]));