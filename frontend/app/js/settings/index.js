require('./company');
require('./invoices');
require('./routes');

(function(app) {})(angular.module('fox.settings', [
    'fox.settings.routes',
    'fox.settings.controllers.company',
    'fox.settings.controllers.invoices'
]));