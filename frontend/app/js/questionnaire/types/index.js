/**
 * Created by quynh on 10/24/2015.
 */
require('./boolean');
require('./single-choice');
require('./multiple-choice');
require('./drop-down');
require('./ChoiceMultiAnswerHorizontal');
require('./ChoiceMultiAnswerSelectBox');
require('./choiceMultiAnswerVertical');
require('./choiceSingleAnswerDropdown');
require('./choiceSingleAnswerHorizontal');
require('./choiceSingleAnswerVertical');
require('./numberAllowcationConstantSum');
require('./openEndedTextCommentBox');
require('./openEndedTextDateTime');
require('./openEndedTextOneLine');
require('./rankOrder');
require('./fileUpload');
require('./datalist');
require('./matrixMultiAnswerPerRow');
require('./matrixSingleAnswerPerRow');
require('./matrixSpreadsheet');

(function (app) {

})(angular.module('fox.questionnaire.types', [
    'fox.questionnaire.types.boolean',
    'fox.questionnaire.types.single-choice',
    'fox.questionnaire.types.multiple-choice',
    'fox.questionnaire.types.drop-down',
    'fox.questionnaire.types.choiceMultiAnswerHorizontal',
    'fox.questionnaire.types.choiceMultiAnswerSelectBox',
    'fox.questionnaire.types.choiceMultiAnswerVertical',
    'fox.questionnaire.types.choiceSingleAnswerDropdown',
    'fox.questionnaire.types.choiceSingleAnswerHorizontal',
    'fox.questionnaire.types.choiceSingleAnswerVertical',
    'fox.questionnaire.types.numberAllowcationConstantSum',
    'fox.questionnaire.types.openEndedTextCommentBox',
    'fox.questionnaire.types.openEndedTextDateTime',
    'fox.questionnaire.types.openEndedTextOneLine',
    'fox.questionnaire.types.rankOrder',
    'fox.questionnaire.types.fileUpload',
    'fox.questionnaire.types.datalist',
    'fox.questionnaire.types.matrixMultiAnswerPerRow',
    'fox.questionnaire.types.matrixSingleAnswerPerRow',
    'fox.questionnaire.types.matrixSpreadsheet',
]));