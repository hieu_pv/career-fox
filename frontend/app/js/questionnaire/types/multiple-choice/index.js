/**
 * Created by quynh on 10/24/2015.
 */

(function (app) {
    app.directive('questionTypeMultipleChoice', theDirective);
    theDirective.$inject = ['API'];

    function theDirective(API) {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/multiple-choice/index.tpl.html',
            controller: function ($scope) {
                $scope.editing = false;

                $scope.add = function () {
                    $scope.control.add(new API.models.questionTypes.Element());
                };

                $scope.remove = function (item) {
                    $scope.control.remove(item);
                };

                $scope.toggle = function () {
                    $scope.editing = !$scope.editing;
                };

                $scope.save = function () {
                    $scope.toggle();
                };
            }
        };

        return directive;
    }

})(angular.module('fox.questionnaire.types.multiple-choice', []));