
(function(app) {
    app.directive('matrixSingleAnswerPerRow', matrixSingleAnswerPerRow);

    function matrixSingleAnswerPerRow() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/matrixSingleAnswerPerRow/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.matrixSingleAnswerPerRow', []));
