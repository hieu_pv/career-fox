(function(app) {
    app.directive('matrixSpreadsheet', matrixSpreadsheet);

    function matrixSpreadsheet() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/matrixSpreadsheet/view.tpl.html',
            link: function(scope) {

            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.matrixSpreadsheet', []));
