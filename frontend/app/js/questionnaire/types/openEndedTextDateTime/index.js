
(function(app) {
    app.directive('openEndedTextDateTime', openEndedTextDateTime);

    function openEndedTextDateTime() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/openEndedTextDateTime/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.openEndedTextDateTime', ['ui.mask']));
