(function(app) {
    app.directive('matrixMultiAnswerPerRow', matrixMultiAnswerPerRow);

    function matrixMultiAnswerPerRow() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/matrixMultiAnswerPerRow/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.matrixMultiAnswerPerRow', []));
