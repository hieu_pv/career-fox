(function(app) {
    app.directive('datalist', datalist);

    function datalist() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/datalist/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.datalist', []));
