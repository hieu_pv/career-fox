(function(app) {
    app.directive('openEndedTextOneLine', openEndedTextOneLine);

    function openEndedTextOneLine() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/openEndedTextOneLine/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.openEndedTextOneLine', ['ui.mask']));
