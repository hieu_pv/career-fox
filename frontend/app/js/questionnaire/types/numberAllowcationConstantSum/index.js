(function(app) {
    app.directive('numberAllowcationConstantSum', numberAllowcationConstantSum);

    function numberAllowcationConstantSum() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/numberAllowcationConstantSum/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.numberAllowcationConstantSum', []));
