
(function(app) {
    app.directive('openEndedTextCommentBox', openEndedTextCommentBox);

    function openEndedTextCommentBox() {
        var directive = {
            restrict: 'EA',
            scope: {
                control: '='
            },
            templateUrl: 'questionnaire/types/openEndedTextCommentBox/view.tpl.html',
            link: function(scope) {
                
            },
        };
        return directive;
    }

})(angular.module('fox.questionnaire.types.openEndedTextCommentBox', []));
