require('./routes');
require('./accounting');
require('./revenue');
require('./invoices');
require('./payment-details');
require('./create-invoices');
require('./subscription');
(function (app) {
})(angular.module('fox.accounting', [
    'fox.accounting.routes',
    'fox.accounting.accounting',
    'fox.accounting.revenue',
    'fox.accounting.invoices',
    'fox.accounting.payment-details',
    'fox.accounting.create-invoices',
    'fox.accounting.subscription'
]));