(function(app) {
    app.controller('AccountingController', controller);
    controller.$inject = [
        '$state',
		'$scope',
		'API'
    ];

    function controller(
        $state, $scope, API
    ) {

        var vm = this;

        vm.currentFilter = -7;

        vm.isFilterActive = function(val) {
            if (val === vm.currentFilter) {
                return 'active';
            }
            return '';
        };

        vm.changeFilter = function(val) {
            vm.currentFilter = val;
        };

        vm.tab = 1;

        vm.isTab = function(tab) {
            return (tab === vm.tab);
        };

        vm.selectTab = function(tab) {
            vm.tab = tab;
        };

        vm.gotoRevenue = function() {
            $state.go('fox.accounting.revenue');
        };
        vm.gotoInvoices = function() {
            $state.go('fox.accounting.invoices');
        };
		
		vm.add = function(a, b) {
			return parseFloat(a) + parseFloat(b);
		};
		
		vm.getRevenue = function(){
			vm.months = [];
			vm.revenue = [];
			$scope.revenueData = [];
			API.invoices.getInvoiceRevenue({'start': $scope.start_date, 'end': $scope.end_date}).then(function(result) {
				for(var i in result){
					if(result[i].amt){
						vm.revenue.push(result[i].amt);
					}
					if(result[i].month){
						vm.months.push(result[i].month);
					}
					if(result[i].month && result[i].amt){
						$scope.revenueData[i] = {'mon':result[i].month,  'revenue' : result[i].amt};
					}
				}

				vm.totalRevenue = vm.revenue.reduce(vm.add, 0);
				$scope.revenueGraphData = {
					labels: vm.months,
					datasets: [{
						label: 'Revenue in $',
						fillColor: 'rgba(151,187,205,0.2)',
						strokeColor: 'rgba(151,187,205,1)',
						pointColor: 'rgba(151,187,205,1)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(151,187,205,1)',
						data: vm.revenue
					}]
				};
				$scope.revenueGraphoptions = {
					maintainAspectRatio: false,
					responsive: true,
					scaleShowGridLines: true,
					scaleGridLineColor: 'rgba(0,0,0,.05)',
					scaleGridLineWidth: 1,
					scaleLabel: function(data){
						return data.value + '$';
					},
					bezierCurve: false,
					bezierCurveTension: 0.4,
					pointDot: true,
					pointDotRadius: 4,
					pointDotStrokeWidth: 1,
					pointHitDetectionRadius: 20,
					datasetStroke: true,
					datasetStrokeWidth: 2,
					datasetFill: true,
					scaleSteps : 10,
					onAnimationProgress: function() {},
					onAnimationComplete: function() {},
					legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
				};
			});
			
			API.jobs.getMonthlyJob().then(function(result) {
				vm.days = [];
				vm.jobs = [];
				for(var i in result){
					if(result[i].tot){
						vm.jobs.push(result[i].tot);
					}
					if(result[i].day){
						vm.days.push(result[i].day);
					}
				}
				
				vm.totalJobs = vm.jobs.reduce(vm.add, 0);
				
				$scope.jobsGraphData = {
					labels: vm.days,
					datasets: [{
						label: 'Jobs',
						fillColor: 'rgba(151,187,205,0.2)',
						strokeColor: 'rgba(151,187,205,1)',
						pointColor: 'rgba(151,187,205,1)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(151,187,205,1)',
						data: vm.jobs
					}]
				};
				$scope.jobsGraphoptions = {
					maintainAspectRatio: false,
					responsive: true,
					scaleShowGridLines: true,
					scaleGridLineColor: 'rgba(0,0,0,.05)',
					scaleGridLineWidth: 1,
					bezierCurve: false,
					bezierCurveTension: 0.4,
					pointDot: true,
					pointDotRadius: 4,
					pointDotStrokeWidth: 1,
					pointHitDetectionRadius: 20,
					datasetStroke: true,
					datasetStrokeWidth: 2,
					datasetFill: true,
					scaleSteps : 10,
					scaleLabel: function(data){
						return data.value;
					},
					onAnimationProgress: function() {},
					onAnimationComplete: function() {},
					legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
				};
			});
		};

		
		
		vm.getRevenue();
    }
})(angular.module('fox.accounting.accounting', []));