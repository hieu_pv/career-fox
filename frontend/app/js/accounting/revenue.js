(function(app) {
    app.controller('AccountingRevenueController', controller);
	app.controller('RevenueCtrl', RevenueCtrl);
    controller.$inject = [
        '$state'
    ];

    function controller(
        $state
    ) {

        var vm = this;
    }
	
	
	RevenueCtrl.$inject = ['$state', '$scope', 'API'];

    function RevenueCtrl($state, $scope, API) {
		
		var vm = this;
		
		$scope.search = function(){
			getRevenue();
		};
		
		getRevenue = function(){
				vm.months = [];
				vm.revenue = [];
				$scope.revenueData = [];
				API.invoices.getInvoiceRevenue({'start': $scope.start_date, 'end': $scope.end_date}).then(function(result) {
					for(var i in result){
						if(result[i].amt){
							vm.revenue.push(result[i].amt);
						}
						if(result[i].month){
							vm.months.push(result[i].month);
						}
						if(result[i].month && result[i].amt){
							$scope.revenueData[i] = {'mon':result[i].month,  'revenue' : result[i].amt};
						}
					}
				});
				
				$scope.data = {
					labels: vm.months,
					datasets: [{
						label: 'Revenue in $',
						fillColor: 'rgba(151,187,205,0.2)',
						strokeColor: 'rgba(151,187,205,1)',
						pointColor: 'rgba(151,187,205,1)',
						pointStrokeColor: '#fff',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(151,187,205,1)',
						data: vm.revenue
					}]
				};

				$scope.options = {
					maintainAspectRatio: false,
					responsive: true,
					scaleShowGridLines: true,
					scaleGridLineColor: 'rgba(0,0,0,.05)',
					scaleGridLineWidth: 1,
					scaleLabel: function(data){
						return data.value + '$';
					},
					bezierCurve: false,
					bezierCurveTension: 0.4,
					pointDot: true,
					pointDotRadius: 4,
					pointDotStrokeWidth: 1,
					pointHitDetectionRadius: 20,
					datasetStroke: true,
					datasetStrokeWidth: 2,
					datasetFill: true,
					scaleSteps : 10,
					onAnimationProgress: function() {},
					onAnimationComplete: function() {},
					legendTemplate: '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
				};
				
		};
		
		getRevenue();
    }
	
})(angular.module('fox.accounting.revenue', []));