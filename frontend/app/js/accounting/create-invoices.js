(function(app) {
    app.controller('CreateInvoicesController', controller);
    controller.$inject = [
        '$state',
        '$scope',
        'dialog',
        'API',
		'allCompanies'
    ];

    function controller($state, $scope, dialog, API, allCompanies) {
        var vm = this;
		$scope.companies = company.data;
        var validStatus = ['pending', 'paid', 'sent', 'cancelled'];
        $scope.validStatus = validStatus;
        $scope.status = validStatus[0];
        $scope.CreateInvoice = function(email, name, amount, status) {
            params = {
                name: name,
                amount: amount,
                status: status,
            };
            API.invoices.create(params)
                .then(function(result) {
                    dialog.alert(null, 'Create invoice successfully');
                });
        };
    }
})(angular.module('fox.accounting.create-invoices', []));
