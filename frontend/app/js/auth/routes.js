(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('auth', {
                    url: '/login',
                    templateUrl: 'auth/login.tpl.html',
                    controller: 'LoginController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.auth.routes', [
    'fox.controllers.login'
]));