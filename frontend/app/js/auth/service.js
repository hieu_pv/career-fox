var User = require('../api/models/user');
(function(app) {
    app.factory('authService', [
        '$q',
        '$resource',
        'auth',
        'apiUrl',
        function(
            $q,
            $resource,
            auth,
            apiUrl
        ) {
            return {
                login: function(email, password) {
                    var url = apiUrl.get('login');
                    var auth = $resource(url, null, {
                        login: {
                            method: 'POST'
                        }
                    });
                    var data = {
                        email: email,
                        password: password
                    };

                    var deferred = $q.defer();
                    auth.login({}, data, function(result) {
                        deferred.resolve(result.token);
                    }, function(err) {
                        deferred.reject(err);
                    });

                    return deferred.promise;
                },

                me: function() {
                    var url = apiUrl.get('me');
                    var resource = $resource(url);

                    var deferred = $q.defer();
                    resource.get({}, function(result) {
                        var user = new User(result.data);
                        deferred.resolve(user);
                        auth.setUser(user);
                    }, function(err) {
                        deferred.reject(err);
                    });

                    return deferred.promise;
                },

                logout: function() {
                    auth.logout();
                }
            };
        }
    ]);
})(angular.module('fox.auth.service', []));