require('./controllers/');

(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.placement-record', {
                    url: '/placement-record',
                    templateUrl: 'placement-record/placement-record.tpl.html',
                    controller: 'PlacementRecordController',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.placement-record.routes', [
    'fox.placement-record.controllers'
]));
