var RecordSet = require('../recordset');
var User = require('../models/user');

(function(app) {
    app.factory('employeeServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList
        };

        var url = apiUrl.get('employees/:id');
        var resource = $resource(url, {}, {
            del: {
                method: 'DELETE'
            }
        });

        function get(id) {
            var params = {
                id: id
            };
            var deferred = $q.defer();
            resource.get(params, function(result) {
                var model = new User(result.data);
                deferred.resolve(model);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function getList(params) {
            var deferred = $q.defer();
            params = params || {};

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new User(item);

                    if (null === model) {
                        continue;
                    }

                    items.push(model);
                }

                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function del(id) {
            var deferred = $q.defer();
            var params = {
                id: id
            };

            resource.del(params, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.employees', []));