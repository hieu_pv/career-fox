var Invoice = require('../models/invoice');

(function(app) {
    app.factory('invoicesServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            getListInvoices: getListInvoices,
            create: create,
			getInvoiceRevenue: getInvoiceRevenue
        };

        var url = apiUrl.get('invoices');
        var invoicesResouce = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            }
        });
		
		var urlRevenue = apiUrl.get('revenue');
        var resourceRevenue = $resource(urlRevenue, {},{
			get: {
                method: 'GET',
				isArray: true
            }
		});

        function getListInvoices(params) {
            var defferred = $q.defer();
            params = params || {};
            invoicesResouce.get(params, function(result) {
                var data = result.data || [];
                var invoices = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Invoice(item);

                    if (null === model) {
                        continue;
                    }

                    invoices.push(model);
                }
                var d = {
                    data: invoices,
                    meta: result.meta
                };
                defferred.resolve(d);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }
		
		function getInvoiceRevenue(params) {
            var defferred = $q.defer();
            params = params || {};
            resourceRevenue.get(params, function(result) {
				defferred.resolve(result);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }

        function create(params) {
            var defferred = $q.defer();
            params = params || {};
            invoicesResouce.create(params, function(result) {
                defferred.resolve(result);
            }, function(err) {
                defferred.reject(err);
            });
            return defferred.promise;
        }
        return service;
    }
})(angular.module('fox.api.invoices', []));
