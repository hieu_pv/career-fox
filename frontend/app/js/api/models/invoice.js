var moment = require('moment');
var BaseModel = require('./base');
var inherits = require('inherits');

function Invoice(options) {
    this.id = null;
    this.name = '';
    this.amount = '';
    this.status = '';
    this.datetime = '';
    BaseModel.call(this, options);
}

inherits(Invoice, BaseModel);

Invoice.prototype.bind = function(options) {
    options = options || {};
    BaseModel.prototype.bind.call(this, options);
};

Invoice.prototype.getId = function() {
    return this.id;
};

Invoice.prototype.getDateTime = function() {
    return moment(this.datetime).format('DD-MM-YYYY');
};

module.exports = Invoice;
