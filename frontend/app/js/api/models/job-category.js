var BaseModel = require('./base');
var inherits = require('inherits');

function JobCategory(options) {
    this.id = null;
    this.title = '';
    this.created_at = '';
    this.updated_at = null;
    BaseModel.call(this, options);
}
inherits(JobCategory, BaseModel);

module.exports = JobCategory;