/**
 * Created by quynh on 10/21/2015.
 */
var BaseModel = require('./base');

var Candidate = require('./candidate');
var inherits = require('inherits');


function PhoneScreen(options) {
    this.id = null;
    this.quick_note = '';
    this.is_shared = false;
    this.invited_video_interview = false;

    BaseModel.call(this, options);
}
inherits(PhoneScreen, BaseModel);

PhoneScreen.prototype.bind = function(options) {
    options = options || {};
    BaseModel.prototype.bind.call(this, options);

    if (options.candidate && options.candidate.data) {
        this.bindCandidate(options.candidate.data);
    }
};

PhoneScreen.prototype.bindCandidate = function(options) {
    this.candidate = new Candidate(options);
};

PhoneScreen.prototype.isInvited = function() {
    return (this.invited_video_interview === true);
};

PhoneScreen.prototype.setInvited = function(val) {
    this.invited_video_interview = val;
};

PhoneScreen.prototype.isShared = function() {
    return (this.is_shared === true);
};

module.exports = PhoneScreen;