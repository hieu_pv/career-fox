var Address = require('./address');
var Candidate = require('./candidate');
var Category = require('./category');
var City = require('./city');
var Company = require('./company');
var Job = require('./job');
var JobCategory = require('./job-category');
var Province = require('./province');
var Question = require('./question');
var User = require('./user');
var PhoneScreen = require('./phone-screen');
var QuestionTypes = require('./questionnaire');
var Survey = require('./survey');

module.exports = {
    Address: Address,
    Candidate: Candidate,
    Category: Category,
    City: City,
    Company: Company,
    Job: Job,
    JoJobCategory: JobCategory,
    Province: Province,
    Question: Question,
    User: User,
    PhoneScreen: PhoneScreen,
    questionTypes: QuestionTypes,
    Survey: Survey
};