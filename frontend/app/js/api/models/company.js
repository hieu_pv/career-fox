var BaseModel = require('./base');
var inherits = require('inherits');

function Company(options) {
    this.id = null;
    this.email = '';
    this.name = '';
    this.description = '';
    this.phone = '';
    this.primary_contact = '';

    BaseModel.call(this, options);
}
inherits(Company, BaseModel);

module.exports = Company;