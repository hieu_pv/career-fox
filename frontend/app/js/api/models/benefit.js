var BaseModel = require('./base');
var inherits = require('inherits');

function Benefit(options) {
    this.id = null;
    this.title = '';
    this.job_id = null;
    this.user_id = null;
    this.created_at = null;
    this.updated_at = null;
    BaseModel.call(this, options);
}
inherits(Benefit, BaseModel);

module.exports = Benefit;