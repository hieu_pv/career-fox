var BaseModel = require('./base');
var inherits = require('inherits');

function Address(options) {
    this.id = null;
    this.address = '';
    this.city = '';
    this.id = '';
    this.phone = '';
    this.province = '';

    BaseModel.call(this, options);
}
inherits(Address, BaseModel);

module.exports = Address;