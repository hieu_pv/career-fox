// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function choiceSingleAnswerDropdown(options) {
    this.id = null;
    this.type = 'choiceSingleAnswerDropdown';
    this.name = 'choiceSingleAnswerDropdown_' + Math.random();
    this.title = '';
    this.value = null;
    this.items = [];

    BaseModel.call(this, options);
}
inherits(choiceSingleAnswerDropdown, BaseModel);


choiceSingleAnswerDropdown.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

choiceSingleAnswerDropdown.prototype.getValue = function() {
    return this.value;
};


choiceSingleAnswerDropdown.prototype.getItems = function() {
    return this.items;
};

choiceSingleAnswerDropdown.prototype.setValue = function(val) {
    this.value = val;
    return this;
};




module.exports = choiceSingleAnswerDropdown;
