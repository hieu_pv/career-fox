// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function openEndedTextDateTime(options) {
    this.id = null;
    this.type = 'openEndedTextDateTime';
    this.name = 'openEndedTextDateTime_' + Math.random();
    this.title = '';
    this.value = '';
    
    BaseModel.call(this, options);
}
inherits(openEndedTextDateTime, BaseModel);


openEndedTextDateTime.prototype.getValue = function() {
    return this.value;
};

openEndedTextDateTime.prototype.setValue = function(val) {
    this.value = val;
};


module.exports = openEndedTextDateTime;
