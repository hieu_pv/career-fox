// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function ChoiceMultiAnswerHorizontal(options) {
    this.id = null;
    this.type = 'ChoiceMultiAnswerHorizontal';
    this.name = 'ChoiceMultiAnswerHorizontal_' + Math.random();
    this.title = '';
    this.values = [];
    this.items = [];
    
    BaseModel.call(this, options);
}
inherits(ChoiceMultiAnswerHorizontal, BaseModel);


ChoiceMultiAnswerHorizontal.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

ChoiceMultiAnswerHorizontal.prototype.getValue = function() {
    return this.values;
};


ChoiceMultiAnswerHorizontal.prototype.getItems = function() {
    return this.items;
};

ChoiceMultiAnswerHorizontal.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};


module.exports = ChoiceMultiAnswerHorizontal;
