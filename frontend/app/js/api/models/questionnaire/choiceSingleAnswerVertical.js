// hieupv.est@gmail.com

var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function choiceSingleAnswerVertical(options) {
    this.id = null;
    this.type = 'choiceSingleAnswerVertical';
    this.name = 'choiceSingleAnswerVertical_' + Math.random();
    this.title = '';
    this.value = null;
    this.items = [];

    BaseModel.call(this, options);
}
inherits(choiceSingleAnswerVertical, BaseModel);


choiceSingleAnswerVertical.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

choiceSingleAnswerVertical.prototype.getValue = function() {
    return this.value;
};


choiceSingleAnswerVertical.prototype.getItems = function() {
    return this.items;
};

choiceSingleAnswerVertical.prototype.setValue = function(val) {
    this.value = val;
    return this;
};




module.exports = choiceSingleAnswerVertical;
