// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');
var ChoiceMultiAnswerHorizontal = require('./ChoiceMultiAnswerHorizontal');

function matrixMultiAnswerPerRow(options) {
    this.id = null;
    this.type = 'matrixMultiAnswerPerRow';
    this.name = 'matrixMultiAnswerPerRow_' + Math.random();
    this.title = '';
    this.values = [];
    this.items = [];

    BaseModel.call(this, options);
}
inherits(matrixMultiAnswerPerRow, BaseModel);


matrixMultiAnswerPerRow.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

matrixMultiAnswerPerRow.prototype.getValue = function() {
    return this.values;
};

matrixMultiAnswerPerRow.prototype.addValue = function (val) {
    var el = new ChoiceMultiAnswerHorizontal(val);
    var items = this.getItems();
    items.forEach(function (item) {
        var element = new Element(item);
        el.add(element);
    });
    this.values.push(el);
    return this;
};

matrixMultiAnswerPerRow.prototype.getItems = function() {
    return this.items;
};

matrixMultiAnswerPerRow.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    var items = this.getItems();
    val.forEach(function(v, k) {
        val[k] = new ChoiceMultiAnswerHorizontal(v);
        items.forEach(function(item) {
            var element = new Element(item);
            val[k].add(element);
        });
    });
    this.values = val;
    return this;
};

module.exports = matrixMultiAnswerPerRow;
