/**
 * Created by quynh on 10/24/2015.
 */
var BaseModel = require('../base');
var inherits = require('inherits');
var SingleChoice = require('./single-choice');

function Dropdown(options) {
    this.id = null;
    this.name = 'dropdown_'+ Math.random();
    this.title = 'Question ?';
    this.value = null;
    this.items = [];

    SingleChoice.call(this, options);

    this.type = 'drop-down';
}
inherits(Dropdown, SingleChoice);

module.exports = Dropdown;