/**
 * Created by quynh on 10/24/2015.
 */
var BaseModel = require('../base');
var inherits = require('inherits');

function Element(options) {
    this.id = null;
    this.title = '';
    this.value = '';

    BaseModel.call(this, options);
}
inherits(Element, BaseModel);

module.exports = Element;