/**
 * Created by quynh on 10/24/2015.
 */

var Element = require('./element');
var SingleChoice = require('./single-choice');
var MultipleChoice = require('./multiple-choice');
var DropDown = require('./drop-down');
var YesNo = require('./boolean');
var ChoiceMultiAnswerHorizontal = require('./ChoiceMultiAnswerHorizontal');
var ChoiceMultiAnswerSelectBox = require('./ChoiceMultiAnswerSelectBox');
var choiceMultiAnswerVertical = require('./choiceMultiAnswerVertical');
var choiceSingleAnswerDropdown = require('./choiceSingleAnswerDropdown');
var choiceSingleAnswerHorizontal = require('./choiceSingleAnswerHorizontal');
var choiceSingleAnswerVertical = require('./choiceSingleAnswerVertical');
var numberAllowcationConstantSum = require('./numberAllowcationConstantSum');
var openEndedTextCommentBox = require('./openEndedTextCommentBox');
var openEndedTextDateTime = require('./openEndedTextDateTime');
var openEndedTextOneLine = require('./openEndedTextOneLine');
var rankOrder = require('./rankOrder');
var fileUpload = require('./fileUpload');
var datalist = require('./datalist');
var matrixMultiAnswerPerRow = require('./matrixMultiAnswerPerRow');
var matrixSingleAnswerPerRow = require('./matrixSingleAnswerPerRow');
var matrixSpreadsheet = require('./matrixSpreadsheet');

module.exports = {
    Element: Element,
    Boolean: YesNo,
    SingleChoice: SingleChoice,
    MultipleChoice: MultipleChoice,
    DropDown: DropDown,
    ChoiceMultiAnswerHorizontal: ChoiceMultiAnswerHorizontal,
    ChoiceMultiAnswerSelectBox: ChoiceMultiAnswerSelectBox,
    choiceMultiAnswerVertical: choiceMultiAnswerVertical,
    choiceSingleAnswerDropdown: choiceSingleAnswerDropdown,
    choiceSingleAnswerHorizontal: choiceSingleAnswerHorizontal,
    choiceSingleAnswerVertical: choiceSingleAnswerVertical,
    numberAllowcationConstantSum: numberAllowcationConstantSum,
    openEndedTextCommentBox: openEndedTextCommentBox,
    openEndedTextDateTime: openEndedTextDateTime,
    openEndedTextOneLine: openEndedTextOneLine,
    rankOrder: rankOrder,
    fileUpload: fileUpload,
    datalist: datalist,
    matrixMultiAnswerPerRow: matrixMultiAnswerPerRow,
    matrixSingleAnswerPerRow: matrixSingleAnswerPerRow,
    matrixSpreadsheet: matrixSpreadsheet,
    createQuestionType: function(type) {
        switch (type) {
            case 'boolean':
                return new YesNo();
            case 'single-choice':
                return new SingleChoice();
            case 'multiple-choice':
                return new MultipleChoice();
            case 'drop-down':
                return new DropDown();
            case 'ChoiceMultiAnswerHorizontal':
                return new ChoiceMultiAnswerHorizontal();
            case 'ChoiceMultiAnswerSelectBox':
                return new ChoiceMultiAnswerSelectBox();
            case 'choiceMultiAnswerVertical':
                return new choiceMultiAnswerVertical();
            case 'choiceSingleAnswerDropdown':
                return new choiceSingleAnswerDropdown();
            case 'choiceSingleAnswerHorizontal':
                return new choiceSingleAnswerHorizontal();
            case 'choiceSingleAnswerVertical':
                return new choiceSingleAnswerVertical();
            case 'numberAllowcationConstantSum':
                return new numberAllowcationConstantSum();
            case 'openEndedTextCommentBox':
                return new openEndedTextCommentBox();
            case 'openEndedTextDateTime':
                return new openEndedTextDateTime();
            case 'openEndedTextOneLine':
                return new openEndedTextOneLine();
            case 'rankOrder':
                return new rankOrder();
            case 'fileUpload':
                return new fileUpload();
            case 'datalist':
                return new datalist();
            case 'matrixMultiAnswerPerRow':
                return new matrixMultiAnswerPerRow();
            case 'matrixSingleAnswerPerRow':
                return new matrixSingleAnswerPerRow();
            case 'matrixSpreadsheet':
                return new matrixSpreadsheet();
            default:
                throw new Error('Invalid question type: ' + type);
        }
    }
};
