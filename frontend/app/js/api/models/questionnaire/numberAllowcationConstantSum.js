// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function numberAllowcationConstantSum(options) {
    this.id = null;
    this.type = 'numberAllowcationConstantSum';
    this.name = 'numberAllowcationConstantSum_' + Math.random();
    this.title = '';
    this.total = null;
    this.items = [];
    
    BaseModel.call(this, options);
}
inherits(numberAllowcationConstantSum, BaseModel);


numberAllowcationConstantSum.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

numberAllowcationConstantSum.prototype.getValue = function() {
    return this.values;
};


numberAllowcationConstantSum.prototype.getItems = function() {
    return this.items;
};

numberAllowcationConstantSum.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};


module.exports = numberAllowcationConstantSum;
