// hieupv.est@gmail.com
var BaseModel = require('../base');
var inherits = require('inherits');
var Element = require('./element');

function ChoiceMultiAnswerSelectBox(options) {
    this.id = null;
    this.type = 'ChoiceMultiAnswerSelectBox';
    this.name = 'ChoiceMultiAnswerSelectBox_' + Math.random();
    this.title = '';
    this.values = [];
    this.items = [];

    BaseModel.call(this, options);
}
inherits(ChoiceMultiAnswerSelectBox, BaseModel);


ChoiceMultiAnswerSelectBox.prototype.add = function(item) {
    if (!(item instanceof Element)) {
        throw new Error('an Element object is expected');
    }
    this.items.push(item);
    return this;
};

ChoiceMultiAnswerSelectBox.prototype.getValue = function() {
    return this.values;
};


ChoiceMultiAnswerSelectBox.prototype.getItems = function() {
    return this.items;
};

ChoiceMultiAnswerSelectBox.prototype.setValue = function(val) {
    if (!Array.isArray(val)) {
        throw new Error('an array is expected');
    }
    this.values = val;
    return this;
};

module.exports = ChoiceMultiAnswerSelectBox;
