var BaseModel = require('./base');
var inherits = require('inherits');

function Question(options) {
    this.id = null;
    this.title = '';
    this.default_answer = '';
    this.job_id = null;
    this.user_id = null;
    this.created_at = null;
    this.updated_at = null;
    BaseModel.call(this, options);
}
inherits(Question, BaseModel);

module.exports = Question;