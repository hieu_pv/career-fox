var RecordSet = require('../recordset');
var Candidate = require('../models/candidate');

(function(app) {
    app.factory('jobVideoInterviewServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList,
            invite: invite,
            reqcandidateid: getReqCandId
        };

        var url = apiUrl.get('jobs/:job_id/video-invitations');
        var resource = $resource(url, {}, {});

        function getReqCandId() {

        }

        function getList(jobId, params) {
            /*reqcandidateid = reqcandidateid || 0;
             if(reqcandidateid == "getReqCadId"){
                 alert(11);
             }*/

            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;

            resource.get(params, function(result) {
                var data = result.data || [];
                if (params.getReqCadId == 'video') {
                    var html_iframe = '';
                    var j = 1;
                    for (var i = 0, n = data.length; i < n; i++) {
                        var video_url = data[i].video.replace(/\\\//g, "/");
                        if (i === 0) {

                            html_iframe += '<video width="320" height="240" controls id="video_' + j + '" >';
                            html_iframe += '<source src="' + video_url + '" type="video/mp4">';
                            html_iframe += '<source src="' + video_url + '" type="video/ogg">';
                            html_iframe += 'Your browser does not support the video tag.';
                            html_iframe += '</video>';
                        } else {
                            html_iframe += '<video width="320" height="240" controls style="display:none" id="video_' + j + '">';
                            html_iframe += '<source src="' + video_url + '" type="video/mp4">';
                            html_iframe += '<source src="' + video_url + '" type="video/ogg">';
                            html_iframe += 'Your browser does not support the video tag.';
                            html_iframe += '</video>';
                        }
                        //html_iframe +='<iframe src="'+video_url+'" class="embed-responsive-item"></iframe>'
                        j++;
                    }
                    var myEl = angular.element(document.querySelector('.video-review-cover'));
                    myEl.html(html_iframe);
                } else {
                    var items = [];
                    for (var k = 0; k < data.length; k++) {
                        var item = data[k];
                        var model = new Candidate(item);
                        items.push(model);
                    }
                    var recordSet = new RecordSet(items, result.meta.pagination);
                    deferred.resolve(recordSet);
                }
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function invite(id) {
            var url = apiUrl.get('video-invitations/:id/questionnaire-invitation');
            var resource = $resource(url, {}, {
                invite: {
                    method: 'POST'
                }
            });

            var params = {
                id: id
            };

            var data = {};

            var deferred = $q.defer();

            resource.invite(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.jobs.video-interview', []));
