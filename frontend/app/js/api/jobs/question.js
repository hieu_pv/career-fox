var RecordSet = require('../recordset');
var Question = require('../models/question');
(function(app) {
    app.factory('questionServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList,
            create: create,
            update: update,
            del: del
        };

        var url = apiUrl.get('jobs/:job_id/questions/:id');
        var resource = $resource(url, {}, {
            create: {
                method: 'POST'
            },
            del: {
                method: 'DELETE'
            },
            update: {
                method: 'PUT'
            }
        });

        function del(jobId, id) {
            var deferred = $q.defer();
            var params = {
                job_id: jobId,
                id: id
            };
            resource.del(params, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function update(jobId, id, data) {
            var deferred = $q.defer();
            var params = {
                job_id: jobId,
                id: id
            };
            resource.update(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function create(jobId, title, default_answer) {
			
            var deferred = $q.defer();
            var params = {
                job_id: jobId
            };
            var data = {
                title: title,
				  default_answer: default_answer
            };
            resource.create(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getList(params) {
            var deferred = $q.defer();
            params = params || {};

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Question(item);

                    if (null === model) {
                        continue;
                    }

                    items.push(model);
                }

                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.questions', []));