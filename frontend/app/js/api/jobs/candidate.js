var RecordSet = require('../recordset');
var Candidate = require('../models/candidate');
var PhoneScreen = require('../models/phone-screen');

(function(app) {
    app.factory('jobCandidatesServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList,
            invitePhone: invitePhone,
            removeInvitePhone: removeInvitePhone,
            getPhoneScreenCandidates: getPhoneScreenCandidates,
            getReviewList: getReviewList,
            getInterviewList: getInterviewList,
            saveInterviewNote: saveInterviewNote,
            getPhoneScreenCandidate: getPhoneScreenCandidate,
            saveQuickNote: saveQuickNote,

            // share phone screen
            share: share,
            // invite phone screen to video
            inviteToVideo: inviteToVideo
        };

        var url = apiUrl.get('jobs/:job_id/candidates/:id');
        var resource = $resource(url, {}, {
            del: {
                method: 'DELETE'
            },
            update: {
                method: 'PUT'
            }
        });

        function invitePhone(jobId, ids) {
            var deferred = $q.defer();

            var url = apiUrl.get('jobs/:job_id/phone-invitations');
            var resource = $resource(url, {}, {
                invite: {
                    method: 'POST'
                }
            });

            var params = {
                job_id: jobId
            };
            var data = {
                candidate_ids: ids
            };
            resource.invite(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function removeInvitePhone(jobId, id) {
            var deferred = $q.defer();

            var url = apiUrl.get('jobs/:job_id/phone-remove');
            var resource = $resource(url, {}, {
                removeInvite: {
                    method: 'POST'
                }
            });

            var params = {
                job_id: jobId
            };
            var data = {
                candidate_id: id
            };
            resource.removeInvite(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getList(jobId, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Candidate(item);
                    items.push(model);
                }

                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        var urlReview = apiUrl.get('jobs/:job_id/review-candidates');
        var resourceReview = $resource(urlReview, {}, {});

        function getReviewList(jobId, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;

            resourceReview.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Candidate(item);
                    items.push(model);
                }

                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getInterviewList(jobId) {
            var deferred = $q.defer();
            var url = apiUrl.get('jobs/:job_id/interview-invitations');
            var resource = $resource(url, {}, {
                get: {
                    method: 'GET'
                }
            });
            var params = {
                job_id: jobId
            };
            resource.get(params, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function saveInterviewNote(interview_invitation_id, quick_note) {
            var deferred = $q.defer();
            var url = apiUrl.get('interview-invitations/:interview_invitation_id');
            var resource = $resource(url, {}, {
                saveQuickNote: {
                    method: 'PUT'
                }
            });
            var params = {
                interview_invitation_id: interview_invitation_id
            };
            var data = {
                quick_note: quick_note
            };
            resource.saveQuickNote(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        var urlPhone = apiUrl.get('jobs/:job_id/phone-invitations/:id');
        var resourcePhone = $resource(urlPhone);

        function getPhoneScreenCandidates(jobId, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;
            params.include = 'candidate';

            resourcePhone.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new PhoneScreen(item);
                    items.push(model);
                }
                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function getPhoneScreenCandidate(jobId, id, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;
            params.id = id;
            params.include = 'candidate';

            resourcePhone.get(params, function(result) {
                var data = result.data || [];
                var model = new PhoneScreen(data);
                deferred.resolve(model);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function saveQuickNote(jobId, candidateId, note) {
            var url = apiUrl.get('jobs/:job_id/phone-invitations/candidate/:id/notes');
            var resource = $resource(url, {}, {
                note: {
                    method: 'POST'
                }
            });


            var deferred = $q.defer();
            var params = {
                job_id: jobId,
                id: candidateId
            };

            var data = {
                note: note
            };

            resource.note(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function share(phonescreenId, employeeIds) {
            var url = apiUrl.get('phone-invitations/:id/share');
            var resource = $resource(url, {}, {
                share: {
                    method: 'POST'
                }
            });

            var deferred = $q.defer();
            var params = {
                id: phonescreenId
            };

            var data = {
                employee_ids: employeeIds
            };

            resource.share(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function inviteToVideo(phoneId) {
            var url = apiUrl.get('phone-invitations/:id/video-invitation');
            var resource = $resource(url, {}, {
                invite: {
                    method: 'POST'
                }
            });

            var deferred = $q.defer();
            var params = {
                id: phoneId
            };

            var data = {};

            resource.invite(params, data, function(result) {
                deferred.resolve(result);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.jobs.candidates', []));
