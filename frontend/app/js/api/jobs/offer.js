var RecordSet = require('../recordset');
var Candidate = require('../models/candidate');

(function(app) {
    app.factory('jobOfferServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory(
        $q,
        $resource,
        apiUrl
    ) {
        var service = {
            list: getList
        };

        var url = apiUrl.get('jobs/:job_id/offer-invitations');
        var resource = $resource(url, {}, {});

        function getList(jobId, params) {
            var deferred = $q.defer();
            params = params || {};

            params.job_id = jobId;

            resource.get(params, function(result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Candidate(item);
                    items.push(model);
                }
                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.jobs.offer', []));