var Paginator = require('./paginator');

function RecordSet(items, paginator) {
    this.items = [];
    this.paginator = null;


    this.setItems(items);
    this.setPaginator(paginator);
}

RecordSet.prototype.getItems = function () {
    return this.items;
};

RecordSet.prototype.setItems = function (items) {
    this.items = items || [];
};

RecordSet.prototype.setPaginator = function (paginator) {
    if (paginator instanceof Paginator) {
        this.paginator = paginator;
    } else if (null === this.paginator) {
        this.paginator = new Paginator(paginator);
    } else {
        this.paginator.bind(paginator);
    }
};

RecordSet.prototype.getPaginator = function () {
    return this.paginator;
};

module.exports = RecordSet;