/**
 * Created by quynh on 10/28/2015.
 */
var RecordSet = require('../recordset');

var Survey = require('../models/survey/survey');

(function (app) {
    app.factory('surveyServices', theFactory);

    theFactory.$inject = [
        '$q',
        '$resource',
        'apiUrl'
    ];

    function theFactory($q,
                        $resource,
                        apiUrl) {
        var service = {
            list: getList
        };

        var url = apiUrl.get('jobs/:job_id/surveys/:id');
        var resource = $resource(url, {}, {
            del: {
                method: 'DELETE'
            }
        });

        function get(jobId, id) {
            var params = {
                job_id: jobId,
                id: id
            };
            var deferred = $q.defer();
            resource.get(params, function (result) {
                var model = new Survey(result.data);
                deferred.resolve(model);
            }, function (err) {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function getList(jobId, params) {
            var deferred = $q.defer();
            params = params || {};
            params.job_id = jobId;

            resource.get(params, function (result) {
                var data = result.data || [];
                var items = [];
                for (var i = 0, n = data.length; i < n; i++) {
                    var item = data[i];
                    var model = new Survey(item);
                    items.push(model);
                }
                var recordSet = new RecordSet(items, result.meta.pagination);
                deferred.resolve(recordSet);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function del(jobId, id) {
            var deferred = $q.defer();
            var params = {
                job_id: jobId,
                id: id
            };

            resource.del(params, function (result) {
                deferred.resolve(result);
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        return service;
    }
})(angular.module('fox.api.surveys', []));
