require('angular');
require('angular-resource');
require('angular-ui-router');
require('angular-ui-mask');
require('angular-strap');
require('angular-strap-tpl');
require('angular-animate');
require('angular-sanitize');
require('jquery');
require('bootstrap');
require('angular-bootstrap-ui');
require('angular-cookies');
require('angular-touch');
require('angular-translate');
require('angular-translate-loader-url');
require('angular-translate-loader-static-files');
require('angular-translate-storage-local');
require('angular-translate-storage-cookie');
require('oclazyload');
require('ngstorage');
require('angular-breadcrumb');
require('angular-loading-bar');
require('angular-scroll');
require('perfect-scrollbar');
require('tc-angular-chartjs');
require('angular-pdf');
require('pdfjs-dist');
// require('angular-sweetalert');
require('ng-dialog');
require('angular-ckeditor');

require('./common/');
require('./storage/');
require('./auth/');
require('./jobs/');
require('./dashboard/');
require('./admindashboard/');
require('./accounting/');
require('./ascentii/');
require('./candidates/');
require('./api/');
require('./error/');
require('./my-jobs/');
require('./settings/');
require('./users/');
require('./questionnaire/');
require('./myaccount/');
require('./placement-record/');

var app = angular.module('fox', [
    'ui.router',
    'ui.router.state',
    'ui.mask',
    'ngResource',
    // 'ui.bootstrap',
    'ui.bootstrap.tabs',
    'ui.bootstrap.pagination',
    'ui.bootstrap.tpls',
    'mgcrea.ngStrap',
    'ngAnimate',
    'ngSanitize',
    // 'oitozero.ngSweetAlert',
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ngDialog',
    'ngTouch',
    'oc.lazyLoad',
    'cfp.loadingBar',
    'ncy-angular-breadcrumb',
    'duScroll',
    'pascalprecht.translate',
    'tc.chartjs',
    'pdf',

    'fox.error',

    // templates
    'fox.common.templates',
    'fox.templates',
    'fox.common.templates',

    'fox.api',
    'fox.common',
    'fox.storage',
    'fox.auth',
    'fox.jobs',
    'fox.dashboard',
    'fox.admindashboard',
    'fox.accounting',
    'fox.candidates',
    'fox.my-jobs',
    'fox.settings',
    'fox.users',
    'fox.questionnaire',
    'fox.myaccount',
    'fox.placement-record',
]);

app.config([
        '$stateProvider',
        '$urlRouterProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$provide',
        '$ocLazyLoadProvider',

        function($stateProvider,
            $urlRouterProvider,
            $controllerProvider,
            $compileProvider,
            $filterProvider,
            $provide,
            $ocLazyLoadProvider) {

            $urlRouterProvider.otherwise('/dashboard/index');

            $stateProvider
                .state('fox', {
                    abstract: true,
                    templateUrl: 'common/index.tpl.html',
                    resolve: {
                        user: function(authService) {
                            return authService.me();
                        }
                    },
                    controller: 'AppController'
                });
        }
    ])
    .controller('AppController', [
        '$scope', '$state', '$rootScope', 'user', 'authService',
        function($scope, $state, $rootScope, user, authService) {
            if (!user) {
                return $state.go('auth');
            }
            $scope.user = user;
            $rootScope.loggedInUser = user;
            $rootScope.$broadcast('UserLogin', user);
            // remove background image for login page
            $rootScope.app.isLogin = false;

            $scope.signOut = function() {
                authService.logout();
                return $state.go('auth');
            };
        }
    ])
    .controller('MainController', [
        '$rootScope', '$scope', '$state', '$translate', '$localStorage', '$window', '$document', '$timeout', 'cfpLoadingBar',
        function($rootScope, $scope, $state, $translate, $localStorage, $window, $document, $timeout, cfpLoadingBar) {
            // Loading bar transition
            // -----------------------------------
            var $win = $($window);

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                //start loading bar on stateChangeStart
                cfpLoadingBar.start();

            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

                //stop loading bar on stateChangeSuccess
                event.targetScope.$watch("$viewContentLoaded", function() {

                    cfpLoadingBar.complete();
                });

                // scroll top the page on change state

                $document.scrollTo(0, 0);

                var emailElm = $document.find('.email-reader');
                if (emailElm.length) {
                    emailElm.animate({
                        scrollTop: 0
                    }, 0);
                }

                // Save the route title
                $rootScope.currTitle = $state.current.title;
            });

            // State not found
            $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {
                //$rootScope.loading = false;
                console.log(unfoundState.to);
                // "lazy.state"
                console.log(unfoundState.toParams);
                // {a:1, b:2}
                console.log(unfoundState.options);
                // {inherit:false} + default options
            });

            $rootScope.pageTitle = function() {
                return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
            };

            // save settings to local storage
            if (angular.isDefined($localStorage.layout)) {
                $scope.app.layout = $localStorage.layout;

            } else {
                $localStorage.layout = $scope.app.layout;
            }
            $scope.$watch('app.layout', function() {
                // save to local storage
                $localStorage.layout = $scope.app.layout;
            }, true);

            //global function to scroll page up
            $scope.toTheTop = function() {

                $document.scrollTopAnimated(0, 600);

            };

            $scope.language = {
                // Handles language dropdown
                listIsOpen: false,
                // list of available languages
                available: {
                    'en': 'English',
                    'it_IT': 'Italiano',
                    'de_DE': 'Deutsch'
                },
                // display always the current ui language
                init: function() {
                    var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                    var preferredLanguage = $translate.preferredLanguage();
                    // we know we have set a preferred one in app.config
                    // $scope.language.selected = $scope.language.available[(proposedLanguage || preferredLanguage)];
                    $scope.language.selected = $scope.language.available.en;
                },
                set: function(localeId, ev) {
                    $translate.use(localeId);
                    $scope.language.selected = $scope.language.available[localeId];
                    $scope.language.listIsOpen = !$scope.language.listIsOpen;
                }
            };

            $scope.language.init();

            // Function that find the exact height and width of the viewport in a cross-browser way
            var viewport = function() {
                var e = window,
                    a = 'inner';
                if (!('innerWidth' in window)) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                return {
                    width: e[a + 'Width'],
                    height: e[a + 'Height']
                };
            };
            // function that adds information in a scope of the height and width of the page
            $scope.getWindowDimensions = function() {
                return {
                    'h': viewport().height,
                    'w': viewport().width
                };
            };
            // Detect when window is resized and set some variables
            $scope.$watch($scope.getWindowDimensions, function(newValue, oldValue) {
                $scope.windowHeight = newValue.h;
                $scope.windowWidth = newValue.w;
                if (newValue.w >= 992) {
                    $scope.isLargeDevice = true;
                } else {
                    $scope.isLargeDevice = false;
                }
                if (newValue.w < 992) {
                    $scope.isSmallDevice = true;
                } else {
                    $scope.isSmallDevice = false;
                }
                if (newValue.w <= 768) {
                    $scope.isMobileDevice = true;
                } else {
                    $scope.isMobileDevice = false;
                }
            }, true);
            // Apply on resize
            $win.on('resize', function() {
                $scope.$apply();
            });
        }
    ]);

app.run([
    '$rootScope',
    '$state',
    '$stateParams',
    function($rootScope,
        $state,
        $stateParams) {

        // Attach Fastclick for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers
        FastClick.attach(document.body);

        // Set some reference to access them from any scope
        $rootScope.$state = $state;

        // GLOBAL APP SCOPE
        // set below basic information
        $rootScope.app = {
            name: 'fox-Fox', // name of your project
            author: 'QBK', // author's name or company name
            description: 'fox Fox', // brief description
            version: '1.0', // current version
            year: ((new Date()).getFullYear()), // automatic current year (for copyright information)
            isMobile: (function() { // true if the browser is a mobile device
                var check = false;
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    check = true;
                }
                return check;
            })(),
            layout: {
                isNavbarFixed: true, //true if you want to initialize the template with fixed header
                isSidebarFixed: true, // true if you want to initialize the template with fixed sidebar
                isSidebarClosed: false, // true if you want to initialize the template with closed sidebar
                isFooterFixed: false, // true if you want to initialize the template with fixed footer
                logo: 'assets/images/logo.png', // relative path of the project logo
            },
            isLogin: false,
            format_date: 'dd-MMM-yyyy'
        };
    }
]);

// Angular-Loading-Bar
// configuration
app.config(['cfpLoadingBarProvider',
    function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;

    }
]);

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
