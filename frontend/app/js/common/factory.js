var Paginator = require('../api/paginator');

(function(app) {
    app.factory('factoryService', theFactory);

    theFactory.$inject = [];

    function theFactory() {
        var service = {
            createPaginator: createPaginator,
            getStatusList: getStatusList
        };

        return service;

        function createPaginator() {
            return new Paginator();
        }

        function getStatusList() {
            return [{
                value: '',
                text: 'None'
            }, {
                value: -1,
                text: 'Deleted'
            }, {
                value: 0,
                text: 'Draft'
            }, {
                value: 1,
                text: 'Active'
            }, {
                value: 2,
                text: 'Closed'
            }, {
                value: 3,
                text: 'Filled'
            }, {
                value: 4,
                text: 'Archived'
            }, {
                value: 5,
                text: 'Re-Opened (Internal)'
            }];
        }
    }
})(angular.module('fox.common.factory', []));