(function(app) {
    app.directive('sortable', [
        '$parse',
        function($parse) {
            return {
                restrict: 'A',

                link: function(scope, iElement, iAttrs) {
                    var sortState = {
                        name: iAttrs.sortField || null,
                        order: iAttrs.sortOrder || 'asc'
                    };

                    function getArrow(order) {
                        return 'sorting_' + order;
                    }

                    function appendArrow(el, order) {
                        el.removeClass('sorting sorting_desc sorting_asc').addClass('sorting');
                        el.siblings('.sorting, .sorting_desc, .sorting_asc').removeClass('sorting sorting_desc sorting_asc').addClass('sorting');
                        var arrow = getArrow(order);
                        el.removeClass('sorting').addClass(arrow);
                    }

                    iElement.find('thead th[data-sort]').each(function() {
                        var $this = $(this);
                        var field = $this.data('sort');
                        var order = $this.data('order');

                        if (field === sortState.name) {
                            appendArrow($this, sortState.order);
                        } else {
                            $this.addClass('sorting');
                        }
                        $this.bind('click', function(e) {
                            scope.onSortClick(e, field);
                        });
                        $this.attr('style', 'cursor: pointer;');
                    });

                    scope.onSortClick = function(e, field) {
                        if (sortState.name === field) {
                            sortState.order = (sortState.order === 'asc') ? 'desc' : 'asc';
                        } else {
                            sortState = {
                                name: field,
                                order: 'asc'
                            };
                        }
                        var fn = $parse(iAttrs.sortable);
                        fn(scope, {
                            data: sortState
                        });

                        var el = $(e.target);
                        appendArrow(el, sortState.order);
                    };
                }
            };
        }
    ]);
})(angular.module('fox.sortable', []));