require('./apiurl');
require('./directives/perfect-scrollbar');
require('./directives/sidebars');
require('./directives/toggle');
require('./directives/empty-links');
require('./directives/select');
require('./directives/pdf-viewer');
require('./directives/sortable');
require('./dialog');
require('./factory');
require('./nl2br');

(function(app) {})(angular.module('fox.common', [
    'fox.common.apiUrl',
    'fox.perfect.scrollbar',
    'fox.sidebar',
    'fox.toggle',
    'fox.empty-links',
    'fox.select',
    'fox.common.dialog',
    'fox.common.factory',
    'fox.common.nl2br',
    'fox.common.directives.pdf-viewer',
    'fox.sortable'
]));