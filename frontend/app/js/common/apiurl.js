(function() {
    'use strict';
    angular.module('fox.common.apiUrl', [])
        .factory('apiUrl', ApiUrl);

    ApiUrl.$inject = [];

    /**
     * Service
     * @returns {Function}
     */
    function ApiUrl() {
        /**
         * Returns absolute URL to API
         * @param string    urls
         * @return string
         */
        return {
            get: function(url) {
                return 'api/v1/' + url;
            },

            getAsset: function(url) {
                return '' + url;
            }
        };
    }
})();