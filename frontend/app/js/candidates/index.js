require('./candidates');
require('./routes');

(function(app) {})(angular.module('fox.candidates', [
    'fox.candidates.routes',
    'fox.candidates.controller'
]));