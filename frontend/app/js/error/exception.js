(function(app) {
    app.config([
        '$logProvider',
        '$provide',
        function($logProvider, $provide) {
            $logProvider.debugEnabled(true);

            $provide.decorator('$exceptionHandler', [
                '$log',
                '$delegate',
                '$injector',
                function($log, $delegate, $injector) {
                    return function(exception, cause) {
                        $log.debug('Exception handler.', exception, cause);

                        //$delegate(exception, cause);
                        var $rootScope = $injector.get("$rootScope");
                        $rootScope.$broadcast('onError', exception, cause);
                    };
                }
            ]);
        }
    ]);

    /**
     * Show Error message
     */
    app.run(errorHandler);
    errorHandler.$inject = ['$rootScope', 'dialog'];

    function errorHandler($rootScope, dialog) {
        $rootScope.$on('onError', function(e, err, cause) {
            console.log(e, err);
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
                showError(err);
            }
        });

        function showError(err) {
            var message = 'Whoops, looks like something went wrong.';
            var alert = dialog.alert('', message);
            alert.closePromise
                .then(function() {
                    $rootScope.hasError = false;
                });
        }
    }
})(angular.module('fox.error.exception', []));