/**
 * Defines error Pages
 */
(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('notfound', {
                    url: '/error404.html',
                    templateUrl: 'error/404.tpl.html',
                    controller: 'errorController'
                })
                .state('error500', {
                    url: '/error500.html',
                    templateUrl: 'error/500.tpl.html',
                    controller: 'ErrorController'
                });
        }
    ]);

    /**
     * Defines Error Controller
     */
    app.controller('errorController', errorController);

    function errorController() {}

})(angular.module('fox.error.routes', [
    'ui.router.state',
    'ui.router'
]));