require('./controllers/');
(function(app) {
    app.config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('fox.myaccount', {
                    abstract: true,
                    template: '<div ui-view></div>',
                    resolve: {}
                })
                .state('fox.myaccount.messages', {
                    url: '/my-account/messages',
                    templateUrl: 'myaccount/templates/messages.tpl.html',
                    controller: 'MyAccountMessageController',
                    controllerAs: 'vm',
                    resolve: {}
                })
                .state('fox.myaccount.messages.view', {
                    url: '/{id:[0-9]{1,}}',
                    templateUrl: 'myaccount/templates/view-message.tpl.html',
                    controller: 'ViewMessageCrtl',
                    controllerAs: 'vm',
                    resolve: {}
                });
        }
    ]);
})(angular.module('fox.myaccount.routes', [
    'fox.myaccount.controllers'
]));
