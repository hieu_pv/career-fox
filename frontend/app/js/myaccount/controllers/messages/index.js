require('./inbox');
require('./view-message');

(function(app) {})(angular.module('fox.myaccount.controllers.messages', [
    'fox.myaccount.controller.messages.inbox',
    'fox.myaccount.controller.messages.view',
]));