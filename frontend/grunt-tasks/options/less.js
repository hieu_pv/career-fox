module.exports = {
    options: {
        compress: true,
        cleancss: true
    },
    frontend: {
        options: {
            // To enable, set sourceMap to true and update sourceMapRootpath based on your install
            // sourceMap needs grunt-contrib-less > 0.9.0
            //sourceMap: true,
            //sourceMapFilename: 'styles.min.css.map'
        },
        files: {
            "<%= build_dir.css%>/custom-styles.min.css": "<%= src_dir.less %>/styles.less"
        }
    }
};