<?php

return [
    'login' => [
        'username' => env('ASCENTII_LOGIN'),
        'password' => env('ASCENTII_PASS'),
        'url' => env('ASCENTII_LOGIN_URL'),
        'link' => env('ASCENTII_LINK')
    ]
];