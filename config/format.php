<?php

return [
    'date' => 'd-M-Y',
    'monthYear' => 'M Y',
    'time' => 'H:i',
    'datetime' => 'd M Y H:i',
];
